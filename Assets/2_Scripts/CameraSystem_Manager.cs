using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cargold;
 
public class CameraSystem_Manager : MonoBehaviour
{
    public static CameraSystem_Manager Instance;

    [SerializeField] private new Camera camera = null;
    [SerializeField] private Transform bgTrf = null;

    public void Init_Func()
    {
        Instance = this;

        this.Deactivate_Func(true);
    }
 
    public void Activate_Func()
    {
        StartCoroutine(FollowChar_Cor());
    }
    private IEnumerator FollowChar_Cor()
    {
        Character_Script _charClass = IngameSystem_Manager.Instance.GetCharClass;

        while (true)
        {
            Vector3 _offsetPos = DataBase_Manager.Instance.camera.offsetPos;
            Vector3 _cameraPos = this.camera.transform.position - _offsetPos;

            Vector3 _charPos = _charClass.transform.position;
            float _followConDist = DataBase_Manager.Instance.camera.followConDist;
            bool _isDist = _cameraPos.CheckDistance_Func(_charPos, _followConDist);
            if (_isDist == false)
            {
                float _followSmoothValue = 0f;

                float _immediateFollowConDist = DataBase_Manager.Instance.camera.immediateFollowConDist;
                if (_cameraPos.CheckDistance_Func(_charPos, _immediateFollowConDist) == true)
                {
                    _followSmoothValue = DataBase_Manager.Instance.camera.followSmoothValue;
                }
                else
                {
                    _followSmoothValue = DataBase_Manager.Instance.camera.immediateFollowSmoothValue;
                }

                Vector3 _lerpPos = Vector3.Lerp(_cameraPos, _charPos, Time.deltaTime * _followSmoothValue);
                Debug_C.Log_Func("_cameraPos : " + _cameraPos + " / _charPos : " + _charPos + " / _lerpPos : " + _lerpPos, Debug_C.PrintLogType.Camera);
                Vector3 _calcCameraPos = _lerpPos + _offsetPos;
                this.camera.transform.position = _calcCameraPos;

                this.bgTrf.localPosition = Vector3.zero - (_calcCameraPos * DataBase_Manager.Instance.camera.farestViewMoveAdjust);
            }

            yield return null;
        }
    }
 
    public void Deactivate_Func(bool _isInit = false)
    {
		if(_isInit == false)
        {
			
        }
    }
}