using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cargold;
 
public class Character_Attack : MonoBehaviour
{
    [SerializeField] private Character_Script charClass = null;

    public void Init_Func()
    {
        this.Deactivate_Func(true);
    }
 
    public void Activate_Func()
    {
        StartCoroutine(Activate_Cor());
    }

    private IEnumerator Activate_Cor()
    {
        while (true)
        {
            yield return null;

            if (Input.GetKeyDown(KeyCode.X) == true)
            {
                if (this.IsAttacking_Func() == true)
                    continue;

                Character_View.AniType _atkAniType = default;
                _atkAniType = this.charClass.IsCollideGround_Func() == true ? Character_View.AniType.Attack : Character_View.AniType.JumpAttack;

                this.charClass.OnPlayAni_Func(_atkAniType, out bool _isPlay);
                if (_isPlay == true)
                {
                    this.charClass.OnAttack_Func();
                }
            }
        }
    }

    public bool IsAttacking_Func()
    {
        Character_View.AniType _currentAniType = this.charClass.GetCurrentAniType_Func();
        return this.IsAttacking_Func(_currentAniType);
    }
    public bool IsAttacking_Func(Character_View.AniType _currentAniType)
    {
        return _currentAniType == Character_View.AniType.Attack || _currentAniType == Character_View.AniType.JumpAttack;
    }

    public void Deactivate_Func(bool _isInit = false)
    {
		if(_isInit == false)
        {
			
        }
    }

    public void CallAni_OnAttack_Func()
    {
        Debug_C.Log_Func("Attack", Debug_C.PrintLogType.Attack);
    }
    public void CallAni_OnAttackDone_Func()
    {
        Debug_C.Log_Func("Attack Done", Debug_C.PrintLogType.Attack);
    }
}