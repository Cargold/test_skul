using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cargold;

public class Character_Collide : MonoBehaviour
{
    [SerializeField] private Character_Script charClass = null;
    private Coroutine sideCor;
    private Coroutine groundCor;
    [SerializeField] private Transform sideCheckGroupTrf = null;
    [SerializeField] private Transform[] sideCheckTrfArr = null;
    [SerializeField] private Transform[] groundCheckTrfArr = null;
    [SerializeField] private List<RaycastHit2D> raycastHitList = null;
    [SerializeField] private bool isCollideWall = false;
    [SerializeField] private bool isCollideGround = false;

    public bool IsCollideWall => this.isCollideWall;
    public bool IsCollideGround => this.isCollideGround;

    public void Init_Func()
    {
        this.raycastHitList = new List<RaycastHit2D>();

        this.Deactivate_Func(true);
    }


    public void Activate_Func()
    {
        this.sideCor = StartCoroutine(CheckWallCol_Cor());
        this.groundCor = StartCoroutine(CheckGroundCol_Cor());
    }
    private IEnumerator CheckWallCol_Cor()
    {
        while (true)
        {
            for (int i = 0; i < this.sideCheckTrfArr.Length; i++)
            {
                Transform _checkTrf = this.sideCheckTrfArr[i];

                this.GetRayResultWall_Func((int _hitNum, List<RaycastHit2D> _hitList) =>
                {
                    if (this.isCollideWall == true)
                    {
                        if (_hitNum == 0)
                        {
                            if (i == this.sideCheckTrfArr.Length - 1)
                            {
                                this.isCollideWall = false;

                                Debug_C.Log_Func("Take Off Wall", Debug_C.PrintLogType.Collide);
                            }
                        }
                        else
                        {
                            return;
                        }
                    }
                    else
                    {
                        if (0 < _hitNum)
                        {
                            this.isCollideWall = true;

                            Debug_C.Log_Func("Collide Wall", Debug_C.PrintLogType.Collide);

                            this.charClass.OnCollideWall_Func();

                            return;
                        }
                    }

                }, _checkTrf);
            }

            yield return null;
        }
    }
    private IEnumerator CheckGroundCol_Cor()
    {
        while (true)
        {
            for (int i = 0; i < this.groundCheckTrfArr.Length; i++)
            {
                bool _isBreak = false;

                this.GetRayResultGround_Func((int _hitNum, List<RaycastHit2D> _hitList) =>
                {
                    if (this.isCollideGround == true)
                    {
                        if (_hitNum == 0)
                        {
                            if (i == this.groundCheckTrfArr.Length - 1)
                            {
                                this.isCollideGround = false;

                                Debug_C.Log_Func("Not Stand", Debug_C.PrintLogType.Collide);
                            }
                        }
                        else
                        {
                            _isBreak = true;

                            return;
                        }
                    }
                    else
                    {
                        if (0 < _hitNum)
                        {
                            this.isCollideGround = true;

                            Debug_C.Log_Func("Stand", Debug_C.PrintLogType.Collide);

                            Character_View.AniType _aniType = this.charClass.IsMove_Func() == true == true ? Character_View.AniType.Walk : Character_View.AniType.Idle;
                            this.charClass.OnPlayAni_Func(_aniType);

                            this.charClass.OnCollideGround_Func();

                            _isBreak = true;

                            return;
                        }
                    }
                });

                if (_isBreak == true)
                    break;
            }

            yield return null;
        }
    }

    public void GetRayResultWall_Func(System.Action<int, List<RaycastHit2D>> _del, float _checkDist = 0f)
    {
        foreach (Transform _checkTrf in this.sideCheckTrfArr)
        {
            if(_checkDist == 0f)
                _checkDist = DataBase_Manager.Instance.character.sideColCheckDist;

            this.GetRayResultWall_Func(_del, _checkTrf, _checkDist);
        }
    }
    private void GetRayResultWall_Func(System.Action<int, List<RaycastHit2D>> _del, Transform _checkTrf, float _checkDist = 0f, float _dirX = 0f)
    {
        Vector3 _checkPos = _checkTrf.position;

        if(_dirX == 0f)
            _dirX = this.charClass.IsRightDir_Func() == true ? 1f : -1f;
        Vector2 _dir = new Vector2(_dirX, 0f);

        ContactFilter2D _contactFilter = DataBase_Manager.Instance.character.contactFilter2D;

        if (_checkDist == 0f)
            _checkDist = DataBase_Manager.Instance.character.sideColCheckDist;

        int _hitNum = Physics2D.Raycast(_checkPos, _dir, _contactFilter, this.raycastHitList, _checkDist);

#if UNITY_EDITOR
        Debug.DrawRay(_checkPos, _dir * _checkDist, Color.black);
#endif

        _del(_hitNum, this.raycastHitList);
    }
    public void GetRayResultGround_Func(System.Action<int, List<RaycastHit2D>> _del, float _checkDist = 0f)
    {
        ContactFilter2D _contactFilter = DataBase_Manager.Instance.character.contactFilter2D;

        if(_checkDist == 0f)
            _checkDist = DataBase_Manager.Instance.character.groundColCheckDist;

        for (int i = 0; i < this.groundCheckTrfArr.Length; i++)
        {
            Transform _checkTrf = this.groundCheckTrfArr[i];
            Vector3 _checkPos = _checkTrf.position;

            int _hitNum = Physics2D.Raycast(_checkPos, Vector2.down, _contactFilter, this.raycastHitList, _checkDist);
#if UNITY_EDITOR
            Debug.DrawRay(_checkPos, Vector2.down * _checkDist, Color.white); 
#endif

            Debug_C.Log_Func("_hitNum : " + _hitNum + " / Hit Cnt : " + this.raycastHitList.Count, Debug_C.PrintLogType.Collide);

            _del(_hitNum, this.raycastHitList);
        }
    }

    public void SetDir_Func(bool _isRight)
    {
        float _xScale = _isRight == true ? 1f : -1f;
        this.sideCheckGroupTrf.localScale = new Vector3(_xScale, 1f, 1f);
    }

    public void Deactivate_Func(bool _isInit = false)
    {
		if(_isInit == false)
        {
			
        }
    }
}