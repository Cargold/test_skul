using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cargold;
 
public class Character_Dash : MonoBehaviour
{
    [SerializeField] private Character_Script charClass = null;
    private int dashableCnt;

    public void Init_Func()
    {
        this.Deactivate_Func(true);
    }
 
    public void Activate_Func()
    {
        this.dashableCnt = DataBase_Manager.Instance.character.dashableCntMax;

        StartCoroutine(Activate_Cor());
    }
    private IEnumerator Activate_Cor()
    {
        while (true)
        {
            yield return null;

            if (this.dashableCnt <= 0)
                continue;

            if(Input.GetKeyDown(KeyCode.Z) == true)
            {
                if (this.charClass.IsAttackState_Func() == true)
                    continue;

                float _dist = DataBase_Manager.Instance.character.dashDistance;

                this.charClass.GetRayResultSide_Func((int _hitNum, List<RaycastHit2D> _hitList) =>
                {
                    if(0 < _hitNum)
                    {
                        if(_hitList[0].distance < _dist)
                            _dist = _hitList[0].distance;
                    }

                }, _dist);

                float _dirX = this.charClass.IsRightDir_Func() == true ? 1f : -1f;
                Vector2 _dir = new Vector2(_dirX, 0f);
                this.transform.Translate(_dir * _dist);

                this.charClass.OnPlayAni_Func(Character_View.AniType.Dash);

                if(this.charClass.IsCollideGround_Func() == false)
                    this.dashableCnt--;
            }
        }
    }

    public void OnCollideGround_Func()
    {
        this.dashableCnt = DataBase_Manager.Instance.character.dashableCntMax;
    }
    public void OnCollideWall_Func()
    {
        this.dashableCnt = DataBase_Manager.Instance.character.dashableCntMax;
    }

    public bool IsDashState_Func()
    {
        return this.charClass.GetCurrentAniType_Func() == Character_View.AniType.Dash;
    }

    public void Deactivate_Func(bool _isInit = false)
    {
		if(_isInit == false)
        {
			
        }
    }
}