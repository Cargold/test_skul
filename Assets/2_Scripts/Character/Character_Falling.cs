using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cargold;
 
public class Character_Falling : MonoBehaviour
{
    [SerializeField] private Character_Script charClass = null;

    public void Init_Func()
    {
        this.Deactivate_Func(true);
    }
 
    public void Activate_Func()
    {
	    
    }
    public void OnFalling_Func(float _power)
    {
        if(this.charClass.IsCollideGround_Func() == false)
        {
            Character_View.AniType _aniType = this.charClass.GetCurrentAniType_Func();
            if (_aniType != Character_View.AniType.Dash
                && this.charClass.IsAttackState_Func(_aniType) == false)
                this.charClass.OnPlayAni_Func(Character_View.AniType.Fall);

            _power *= Time.deltaTime;

            float _nearestDist = 999f;

            this.charClass.GetRayResultGround_Func((int _hitNum, List<RaycastHit2D> _hitList) =>
            {
                Debug_C.Log_Func("_hitNum : " + _hitNum, Debug_C.PrintLogType.Falling);

                if (0 < _hitNum)
                {
                    foreach (RaycastHit2D _hit in _hitList)
                    {
                        float _dist = this.transform.position.y - _hit.point.y;

                        Debug_C.Log_Func("This Y : " + this.transform.position.y + " / Hit Y :" + _hit.point.y + " / _dist : " + _dist, Debug_C.PrintLogType.Falling);
                        if (_dist < _nearestDist)
                        {
                            _nearestDist = _dist;
                        }
                    }
                }
            }, _power);

            Debug_C.Log_Func("_nearestDist : " + _nearestDist + " / _power : " + _power, Debug_C.PrintLogType.Falling);

            if (_nearestDist < _power)
            {
                _power = _nearestDist - DataBase_Manager.Instance.character.groundColCheckDist;
            }

            this.transform.Translate(_power * Vector2.down);
        }
    }
 
    public void Deactivate_Func(bool _isInit = false)
    {
		if(_isInit == false)
        {
			
        }
    }
}