using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cargold;

public class Character_Jump : MonoBehaviour
{
    [SerializeField] private Character_Script charClass = null;
    [SerializeField] private bool isJumpable;
    private Coroutine jumpCor;

    public bool IsJumping => this.jumpCor != null;

    public void Init_Func()
    {
        this.Deactivate_Func(true);
    }
 
    public void Activate_Func()
    {
        this.isJumpable = true;

        StartCoroutine(Activate_Cor());
    }

    private IEnumerator Activate_Cor()
    {
        while (true)
        {
            if(this.charClass.IsCollideGround_Func() == true)
            {
                if (Input.GetKeyDown(KeyCode.Space) == true)
                {
                    if (this.isJumpable == true)
                    {
                        Debug_C.Log_Func("jump", Debug_C.PrintLogType.Jump);

                        this.isJumpable = false;

                        if (this.jumpCor != null)
                        {
                            StopCoroutine(this.jumpCor);
                            this.jumpCor = null;
                        }

                        this.jumpCor = StartCoroutine(OnJump_Cor());
                    }
                }
            }

            yield return null;
        }
    }
    private IEnumerator OnJump_Cor()
    {
        this.charClass.OnPlayAni_Func(Character_View.AniType.Jump);

        float _jumpValue = DataBase_Manager.Instance.character.jumpHeight;
        while (0f < _jumpValue)
        {
            Debug_C.Log_Func("_jumpValue : " + _jumpValue, Debug_C.PrintLogType.Jump);

            this.transform.Translate(Vector2.up * _jumpValue * Time.deltaTime);

            _jumpValue -= DataBase_Manager.Instance.character.jumpDecrease;

            yield return null;
        }

        this.jumpCor = null;
    }

    public void OnCollideGround_Func()
    {
        this.isJumpable = true;

        if (this.jumpCor != null)
        {
            StopCoroutine(this.jumpCor);
            this.jumpCor = null;
        }
    }

    public void Deactivate_Func(bool _isInit = false)
    {
		if(_isInit == false)
        {
			
        }
    }
}