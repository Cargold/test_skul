using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cargold;

public class Character_Move : MonoBehaviour
{
    [SerializeField] private Character_Script charClass = null;
    private Coroutine moveCor;

    public bool IsMove => this.moveCor != null;

    public void Init_Func()
    {
        this.Deactivate_Func(true);
    }

    public void Activate_Func()
    {
        StartCoroutine(Activate_Cor());
    }
    private IEnumerator Activate_Cor()
    {
        while (true)
        {
            yield return null;

            if (Input.GetKeyUp(KeyCode.RightArrow) == true)
            {
                Debug_C.Log_Func("Key Up, Right", Debug_C.PrintLogType.Move);

                this.OnMoveStop_Func(true);
            }

            else if (Input.GetKeyUp(KeyCode.LeftArrow) == true)
            {
                Debug_C.Log_Func("Key Up, Left", Debug_C.PrintLogType.Move);

                this.OnMoveStop_Func(false);
            }
            else if (Input.GetKey(KeyCode.LeftArrow) == true)
            {
                Debug_C.Log_Func("Key Down, Left", Debug_C.PrintLogType.Move);

                this.OnMove_Func(false);
            }

            else if (Input.GetKey(KeyCode.RightArrow) == true)
            {
                Debug_C.Log_Func("Key Down, Right", Debug_C.PrintLogType.Move);

                this.OnMove_Func(true);
            }
        }
    }
    private void OnMove_Func(bool _isRight)
    {
        if (this.charClass.IsAttackState_Func() == false
            && this.charClass.IsDashState_Func() == false)
        {
            Debug_C.Log_Func("OnMove : " + _isRight, Debug_C.PrintLogType.Move);

            this.charClass.SetDir_Func(_isRight);

            if (this.charClass.IsCollideGround_Func() == true)
                this.charClass.OnPlayAni_Func(Character_View.AniType.Walk);

            this.OnMoveStop_Func();

            if(this.charClass.IsCollideWall_Func() == false)
                this.moveCor = StartCoroutine(this.OnMove_Cor(_isRight));
        }
    }
    private void OnMoveStop_Func(bool _isRight)
    {
        if (this.charClass.IsRightDir_Func() == _isRight)
        {
            Debug_C.Log_Func("OnMoveStop_Func : " + _isRight, Debug_C.PrintLogType.Move);

            if (this.charClass.IsCollideGround_Func() == true
                && this.charClass.IsAttackState_Func() == false)
                this.charClass.OnPlayAni_Func(Character_View.AniType.Idle);

            this.OnMoveStop_Func();
        }
    }

    private IEnumerator OnMove_Cor(bool _isRight)
    {
        while (true)
        {
            yield return null;

            if (this.charClass.IsCollideWall_Func() == false)
            {
                if (this.charClass.IsCollideGround_Func() == true)
                {
                    if (this.charClass.IsAttackState_Func() == true)
                        continue;

                    if (this.charClass.GetCurrentAniType_Func() != Character_View.AniType.Walk)
                        this.charClass.OnPlayAni_Func(Character_View.AniType.Walk);
                }


                float _moveSpeed = DataBase_Manager.Instance.character.moveSpeed;
                float _dist = _moveSpeed * Time.deltaTime;
                
                this.charClass.GetRayResultSide_Func((int _hitNum, List<RaycastHit2D> _hitList) =>
                {
                    if(0 < _hitNum)
                        _dist = _hitList[0].distance;

                    float _dirX = this.charClass.IsRightDir_Func() == true ? 1f : -1f;
                    Vector2 _dir = new Vector2(_dirX, 0f);
                    this.transform.Translate(_dir * _dist);

                }, _dist);
            }
            else
            {
                this.OnMoveStop_Func();
            }
        }
    }

    public void OnMoveStop_Func()
    {
        if (this.moveCor != null)
        {
            StopCoroutine(this.moveCor);
            this.moveCor = null;
        }
    }

    public void Deactivate_Func(bool _isInit = false)
    {
		if(_isInit == false)
        {
			
        }
    }
}