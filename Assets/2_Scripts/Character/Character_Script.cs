using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cargold;
 
public class Character_Script : MonoBehaviour, GravitySystem_Manager.IGravity
{
    [SerializeField] private Character_View viewClass = null;
    [SerializeField] private Character_Move moveClass = null;
    [SerializeField] private Character_Collide collideClass = null;
    [SerializeField] private Character_Jump jumpClass = null;
    [SerializeField] private Character_Falling fallingClass = null;
    [SerializeField] private Character_Attack attackClass = null;
    [SerializeField] private Character_Dash dashClass = null;

    public void Init_Func()
    {
        this.viewClass.Init_Func();
        this.moveClass.Init_Func();
        this.collideClass.Init_Func();
        this.jumpClass.Init_Func();
        this.fallingClass.Init_Func();
        this.attackClass.Init_Func();
        this.dashClass.Init_Func();
    }

    public void Activate_Func()
    {
        this.viewClass.Activate_Func();
        this.moveClass.Activate_Func();
        this.collideClass.Activate_Func();
        this.jumpClass.Activate_Func();
        this.fallingClass.Activate_Func();
        this.attackClass.Activate_Func();
        this.dashClass.Activate_Func();

        GravitySystem_Manager.Instance.Subscribe_Func(this);
    }

    public void OnPlayAni_Func(Character_View.AniType _aniType)
    {
        this.OnPlayAni_Func(_aniType, out _);
    }
    public void OnPlayAni_Func(Character_View.AniType _aniType, out bool _isPlay)
    {
        this.viewClass.OnPlayAni_Func(_aniType, out _isPlay);
    }
    public void OnCollideGround_Func()
    {
        this.jumpClass.OnCollideGround_Func();
        this.dashClass.OnCollideGround_Func();
    }
    public void OnCollideWall_Func()
    {
        this.dashClass.OnCollideWall_Func();
    }
    public void OnAttack_Func()
    {
        
    }

    public bool IsMove_Func()
    {
        return this.moveClass.IsMove;
    }
    public bool IsCollideWall_Func()
    {
        return this.collideClass.IsCollideWall;
    }
    public bool IsCollideGround_Func()
    {
        return this.collideClass.IsCollideGround;
    }
    public bool IsRightDir_Func()
    {
        return this.viewClass.IsRightDir_Func();
    }
    public bool IsAttackState_Func()
    {
        return this.attackClass.IsAttacking_Func();
    }
    public bool IsAttackState_Func(Character_View.AniType _currentAniType)
    {
        return this.attackClass.IsAttacking_Func(_currentAniType);
    }
    public bool IsDashState_Func()
    {
        return this.dashClass.IsDashState_Func();
    }
    public void GetRayResultSide_Func(System.Action<int, List<RaycastHit2D>> _del, float _checkDist = 0f)
    {
        this.collideClass.GetRayResultWall_Func(_del, _checkDist);
    }
    public void GetRayResultGround_Func(System.Action<int, List<RaycastHit2D>> _del, float _checkDist = 0f)
    {
        this.collideClass.GetRayResultGround_Func(_del, _checkDist);
    }
    public Character_View.AniType GetCurrentAniType_Func()
    {
        return this.viewClass.GetCurrentAniType_Func();
    }

    public void SetDir_Func(bool _isRight)
    {
        this.collideClass.SetDir_Func(_isRight);

        this.viewClass.SetDir_Func(_isRight);
    }

    public void Deactivate_Func(bool _isInit = false)
    {
		if(_isInit == false)
        {
			
        }
    }

    string GravitySystem_Manager.IGravity.GetName_Func()
    {
        return this.gameObject.name;
    }
    bool GravitySystem_Manager.IGravity.IsFalling_Func()
    {
        if(this.collideClass.IsCollideGround == false)
        {
            if(this.dashClass.IsDashState_Func() == false)
            {
                if (this.jumpClass.IsJumping == false)
                {
                    return true;
                }
            }
        }

        return false;
    }
    void GravitySystem_Manager.IGravity.OnFalling_Func(float _power)
    {
        this.fallingClass.OnFalling_Func(_power);
    }
}