using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cargold;

public class Character_View : MonoBehaviour
{
    public const string IdleAni = "Idle";
    public const string WalkAni = "Walk";
    public const string JumpAni = "Jump";
    public const string FallAni = "Fall";
    public readonly string[] AttackAniArr = { "Attack_A", "Attack_B" };
    public const string JumpAttackAni = "JumpAttack";
    public const string DashAni = "Dash";

    [SerializeField] private Transform bodyTrf = null;
    [SerializeField] private Animator anim = null;
    [SerializeField] private AniData[] aniDataArr = null;
    [SerializeField] private List<AnimatorClipInfo> clipInfoList = null;
    private Dictionary<AnimationClip, AniType> aniTypeDic;

    public void Init_Func()
    {
        this.aniTypeDic = new Dictionary<AnimationClip, AniType>();

        foreach (AniData _aniData in this.aniDataArr)
        {
            AnimationClip _clip = _aniData.clip;
            AniType _aniType = _aniData.aniType;
            this.aniTypeDic.Add_Func(_clip, _aniType);
        }

        this.clipInfoList = new List<AnimatorClipInfo>();

        this.Deactivate_Func(true);
    }
 
    public void Activate_Func()
    {
	    
    }

    public void SetDir_Func(bool _isRight)
    {
        if (this.IsRightDir_Func() == _isRight)
            return;

        float _xScale = _isRight == true ? 1f : -1f;
        this.bodyTrf.localScale = new Vector3(_xScale, 1f, 1f);
    }

    public bool IsRightDir_Func()
    {
        return 0f < this.bodyTrf.localScale.x;
    }

    public void OnPlayAni_Func(AniType _aniType, out bool _isPlay)
    {
        Debug_C.Log_Func("_aniType : " + _aniType, Debug_C.PrintLogType.View);

        if (this.GetCurrentAniType_Func() == _aniType && _aniType != AniType.Attack)
        {
            _isPlay = false;

            return;
        }

        string _aniTrigger = string.Empty;

        switch (_aniType)
        {
            default:
            case AniType.None:
            case AniType.Idle:
                {
                    _aniTrigger = IdleAni;
                }
                break;

            case AniType.Walk:
                {
                    _aniTrigger = WalkAni;
                }
                break;

            case AniType.Jump:
                {
                    _aniTrigger = JumpAni;
                }
                break;

            case AniType.Fall:
                {
                    _aniTrigger = FallAni;
                }
                break;

            case AniType.Attack:
                {
                    _aniTrigger = AttackAniArr.GetRandItem_Func();
                }
                break;

            case AniType.JumpAttack:
                {
                    _aniTrigger = JumpAttackAni;
                }
                break;

            case AniType.Dash:
                {
                    _aniTrigger = DashAni;
                }
                break;
        }

        this.anim.SetTrigger(_aniTrigger);

        _isPlay = true;
    }

    public AniType GetCurrentAniType_Func()
    {
        this.anim.GetCurrentAnimatorClipInfo(0, this.clipInfoList);

        AnimatorClipInfo _clipInfo = this.clipInfoList[0];
        Debug_C.Log_Func("Current Ani Clip : " + _clipInfo.clip, Debug_C.PrintLogType.View);
        return this.aniTypeDic.GetValue_Func(_clipInfo.clip);
    }
 
    public void Deactivate_Func(bool _isInit = false)
    {
		if(_isInit == false)
        {
			
        }
    }

    public enum AniType
    {
        None = 0,

        Idle = 10,
        Walk = 20,
        Jump = 30,
        Fall = 40,
        Attack = 50,
        JumpAttack = 60,
        Dash = 70,
    }

    [System.Serializable]
    public class AniData
    {
        public AnimationClip clip;
        public AniType aniType;
    }
}