using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cargold;

namespace DB
{
    public class DB_Camera : MonoBehaviour
    {
        public Vector3 offsetPos;
        public float followSmoothValue;
        public float followConDist;
        public float immediateFollowConDist;
        public float immediateFollowSmoothValue;
        public float farestViewMoveAdjust;

        public void Init_Func()
        {

        }
    } 
}