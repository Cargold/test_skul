using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cargold;

namespace DB
{
    public class DB_Character : MonoBehaviour
    {
        public float moveSpeed = 1f;
        public float sideColCheckDist = 1f;
        public float groundColCheckDist = 1f;
        public float jumpHeight = 10f;
        public float jumpDecrease = 1f;
        public float dashDistance = 1f;
        public int dashableCntMax = 2;
        public ContactFilter2D contactFilter2D;

        private int obstacleLayerMask;
        public int GetObstacleLayerMask => this.obstacleLayerMask;

        public void Init_Func()
        {
            this.obstacleLayerMask = LayerMask.NameToLayer("Obstacle");
        }
    } 
}