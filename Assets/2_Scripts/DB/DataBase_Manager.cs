using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cargold;
using DB;

public class DataBase_Manager : Cargold.FrameWork.DataBase_Manager
{
    public static new DataBase_Manager Instance;

    [SerializeField] private Debug_C.PrintLogType logType;

    public DB_Character character;
    public new DB_Camera camera;
    public DB_Gravity gravity;

    protected override Debug_C.PrintLogType GetPrintLogType => this.logType;

    public override void Init_Func(int _layer)
    {
        base.Init_Func(_layer);

        if(_layer == 0)
        {
            Instance = this;

            this.character.Init_Func();
            this.camera.Init_Func();
            this.gravity.Init_Func();
        }
    }
}