using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cargold;
 
public class GameSystem_Manager : Cargold.FrameWork.GameSystem_Manager
{
    public static GameSystem_Manager Instance;

    [SerializeField] private DataBase_Manager dataBase_Manager = null;
    [SerializeField] private IngameSystem_Manager ingameSystem_Manager = null;

    protected override void Init_Func()
    {
        base.Init_Func();

        Instance = this;

        this.dataBase_Manager.Init_Func(0);
        this.ingameSystem_Manager.Init_Func();
    }

    private void Start()
    {
        IngameSystem_Manager.Instance.Activate_Func();
    }
}