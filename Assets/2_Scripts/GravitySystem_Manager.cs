using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cargold;

public class GravitySystem_Manager : MonoBehaviour
{
    public static GravitySystem_Manager Instance;

    private List<IGravity> gravityList;
    private Dictionary<IGravity, FallingData> fallingTargetDic;
    private List<IGravity> standList;
    private List<FallingData> fallingDataList;

    public void Init_Func()
    {
        Instance = this;

        this.gravityList = new List<IGravity>();
        this.fallingTargetDic = new Dictionary<IGravity, FallingData>();
        this.standList = new List<IGravity>();
        this.fallingDataList = new List<FallingData>();

        this.Deactivate_Func(true);
    }
 
    public void Activate_Func()
    {
        StartCoroutine(OnGravity_Cor());
    }

    private IEnumerator OnGravity_Cor()
    {
        while (true)
        {
            // 중력 영향 받을 애들 분류
            for (int i = this.gravityList.Count - 1; i >= 0; i--)
            {
                IGravity _iGravity = this.gravityList[i];
                if (_iGravity.IsFalling_Func() == true)
                {
                    this.gravityList.Remove(_iGravity);

                    FallingData _fallingData = null;
                    if(0 < this.fallingDataList.Count)
                    {
                        _fallingData = this.fallingDataList[0];
                        this.fallingDataList.RemoveAt(0);
                    }
                    else
                    {
                        _fallingData = new FallingData();
                    }

                    this.fallingTargetDic.Add(_iGravity, _fallingData);
                }
            }

            // 중력 적용
            DB_Gravity _gravity = DataBase_Manager.Instance.gravity;
            foreach (var _item in this.fallingTargetDic)
            {
                if(_item.Key.IsFalling_Func() == false)
                {
                    this.standList.Add(_item.Key);
                    continue;
                }

                float _fallingSpeed = 0f;

                if(0f < _item.Value.power)
                {
                    _fallingSpeed = (_item.Value.power + _gravity.fallingIncreaseSpeed);

                    if (_gravity.fallingMaxSpeed < _fallingSpeed)
                        _fallingSpeed = _gravity.fallingMaxSpeed;
                }
                else
                {
                    _fallingSpeed = _gravity.fallingInitSpeed;
                }

                _item.Key.OnFalling_Func(_fallingSpeed);

                _item.Value.power = _fallingSpeed;
            }

            // 땅에 발 닿은 애들 정리
            if(0 < this.standList.Count)
            {
                foreach (IGravity _stand in this.standList)
                {
                    this.gravityList.AddNewItem_Func(this.standList);

                    this.fallingTargetDic.TryGetValue(_stand, out FallingData _faliingData);
                    this.fallingTargetDic.Remove(_stand);

                    _faliingData.power = 0f;

                    this.fallingDataList.AddNewItem_Func(_faliingData);
                }

                this.standList.Clear();
            }

            yield return null;
        }
    }

    public void Subscribe_Func(IGravity _iGravity)
    {
        this.gravityList.AddNewItem_Func(_iGravity);
    }
    public void Unsubscribe_Func(IGravity _iGravity)
    {
        if(this.gravityList.Contains(_iGravity) == true)
        {
            this.gravityList.Remove(_iGravity);
        }
        else
        {
            if(this.fallingTargetDic.Remove(_iGravity) == false)
            {
                Debug_C.Error_Func("? : " + _iGravity.GetName_Func());
            }
        }
    }
 
    public void Deactivate_Func(bool _isInit = false)
    {
		if(_isInit == false)
        {
			
        }
    }
    
    public interface IGravity
    {
        string GetName_Func();
        bool IsFalling_Func();
        void OnFalling_Func(float _power);
    }

    public class FallingData
    {
        public float power;
    }
}