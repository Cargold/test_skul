using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cargold;
 
public class IngameSystem_Manager : MonoBehaviour
{
    public static IngameSystem_Manager Instance;

    [SerializeField] private CameraSystem_Manager cameraSystem_Manager = null;
    [SerializeField] private Character_Script charClass = null;
    [SerializeField] private GravitySystem_Manager gravitySystem_Manager = null;

    public Character_Script GetCharClass => charClass;

    public void Init_Func()
    {
        Instance = this;

        this.cameraSystem_Manager.Init_Func();
        this.charClass.Init_Func();
        this.gravitySystem_Manager.Init_Func();

        this.Deactivate_Func(true);
    }
 
    public void Activate_Func()
    {
        this.charClass.Activate_Func();
        this.cameraSystem_Manager.Activate_Func();
        this.gravitySystem_Manager.Activate_Func();
    }
 
    public void Deactivate_Func(bool _isInit = false)
    {
		if(_isInit == false)
        {
			
        }
    }
}