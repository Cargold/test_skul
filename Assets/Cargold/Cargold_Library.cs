﻿using Cargold.FrameWork;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Internal;
using UnityEngine.UI;
//using DG.Tweening;

// 0.4.9 Ver ('21.08.20)
// Enum_C 갱신
// 제외하고 싶은 멤버 ID를 골라서 부분 배열로 반환 받기

// 스파인 확장 기능 추가

// String 확장 함수 추가

// Extenstion Method    
namespace Cargold
{
    public static class Cargold_Library
    {
        #region Animation Group
        public static void Play_Func(this Animation _anim, bool _isRewind = false, bool _isImmediatly = false, float _speed = 1f)
        {
            _anim.Play_Func(_anim.clip, _isRewind, _isImmediatly, _speed);
        }
        public static void Play_Func(this Animation _anim, AnimationClip _clip, bool _isRewind = false, bool _isImmediatly = false, float _speed = 1f)
        {
            string _clipName = _clip.name;

            _anim.Play_Func(_clipName, _isRewind, _isImmediatly, _speed);
        }
        public static void Play_Func(this Animation _anim, string _clipName, bool _isRewind = false, bool _isImmediatly = false, float _speed = 1f)
        {
            // _isImmediatly를 사용할 경우 애니메이션 이벤트 함수는 작동 안 됨

            if (_anim != null)
            {
                if (_anim.gameObject.activeSelf == false)
                    _anim.gameObject.SetActive(true);

                AnimationClip _clip = _anim.GetClip(_clipName);
                if (_clip != null)
                {
                    float _time = 0f;

                    if (_isImmediatly == false)
                    {
                        if (_isRewind == false)
                        {
                            _time = 0f;
                        }
                        else
                        {
                            _speed *= -1f;

                            _time = _anim[_clipName].length;
                        }
                    }
                    else
                    {
                        _speed = 0f;

                        if (_isRewind == false)
                        {
                            _time = _anim[_clipName].length;
                        }
                        else
                        {
                            _time = 0f;
                        }
                    }

                    _anim[_clipName].speed = _speed;
                    _anim[_clipName].time = _time;
                    _anim.Play(_clipName);
                }
                else
                {
                    Debug_C.Error_Func("애니메이션 클립이 없습니다. : " + _clipName);
                }
            }
            else
            {
                Debug_C.Error_Func("애니메이션 컴포넌트가 비어있습니다. : " + _anim.gameObject.name);
            }
        }
        #endregion
        #region Array Group
        public static T GetLastItem_Func<T>(this T[] _arr)
        {
            return _arr[_arr.Length - 1];
        }
        public static T GetRandItem_Func<T>(this T[] _arr)
        {
            int _temp = 0;
            return _arr.GetRandItem_Func(out _temp);
        }
        public static T GetRandItem_Func<T>(this T[] _arr, int _startIndex = 0, int _lastIndex = -1)
        {
            int _temp = 0;
            return _arr.GetRandItem_Func(out _temp, _startIndex, _lastIndex);
        }
        public static T GetRandItem_Func<T>(this T[] _arr, out int _randID, int _startIndex = 0, int _lastIndex = -1)
        {
            if (_arr == null)
            {
                Debug_C.Error_Func("배열이 비어있습니다.");

                _randID = 0;

                return default;
            }
            else
            {
                if (1 < _arr.Length)
                {
                    if (_lastIndex == -1)
                        _lastIndex = _arr.Length;

                    _randID = UnityEngine.Random.Range(_startIndex, _lastIndex);

                    T _randItem = _arr[_randID];

                    return _randItem;
                }
                else
                {
                    _randID = 0;

                    return _arr[0];
                }
            }
        }
        public static T[] GetRandomPick_Func<T>(this T[] _arr, int _pickNum)
        {
            if (_pickNum <= _arr.Length)
            {
                T[] _valueTypeArr = new T[_pickNum];

                GetRandomPickNonAlloc_Func(_arr, _pickNum, _valueTypeArr);

                return _valueTypeArr;
            }
            else
            {
                Debug.LogError("_pickNum : " + _pickNum);
                Debug.LogError("_arr.Length : " + _arr.Length);
                Debug_C.Error_Func("RandomPick 숫자에 비해 Array의 Item 개수가 부족합니다.");

                return null;
            }
        }
        public static void GetRandomPickNonAlloc_Func<T>(this T[] _arr, int _pickNum, T[] _pickedArr)
        {
            if (_pickNum <= _pickedArr.Length)
            {
                for (int i = 0; i < _pickNum; i++)
                {
                    int _randomPickIndex = UnityEngine.Random.Range(0, _arr.Length - i);
                    _pickedArr[i] = _arr[_randomPickIndex];

                    _arr.Swap_Func(_randomPickIndex, _arr.Length - i - 1);
                }
            }
            else
            {
                Debug.LogError("_pickNum : " + _pickNum);
                Debug.LogError("_arr.Length : " + _arr.Length);
                Debug_C.Error_Func("RandomPick 숫자에 비해 Array의 Item 개수가 부족합니다.");
            }
        }
        public static bool SetSameLength_Func<T>(this T[] _arr, T[] _setArr)
        {
            if (_arr.Length == _setArr.Length)
            {
                for (int i = 0; i < _arr.Length; i++)
                    _arr[i] = _setArr[i];

                return true;
            }
            else
            {
                return false;
            }
        }

        // 이거 밸류 타입도 문제 없는지 확인 필요함
        public static void Swap_Func<T>(this T[] _arr, int _swapIndex1, int _swapIndex2)
        {
            if (_swapIndex1 != _swapIndex2)
            {
                if (_swapIndex1 < _arr.Length && _swapIndex2 < _arr.Length && 0 <= _swapIndex1 && 0 <= _swapIndex2)
                {
                    T _temp = _arr[_swapIndex1];
                    _arr[_swapIndex1] = _arr[_swapIndex2];
                    _arr[_swapIndex2] = _temp;
                }
                else
                {
                    Debug_C.Error_Func("Swap하려는 배열의 크기는 " + _arr.Length + ". 하지만 접근하려는 Index는 " + _swapIndex1 + ", 그리고 " + _swapIndex2);
                }
            }
            else
            {

            }
        }

        public static int GetIndex_Func<T>(this T[] _arr, T _targetItem, bool _isSearchAscending = true) where T : class
        {
            int _index = -1;

            if (_isSearchAscending == true)
            {
                for (int i = 0; i < _arr.Length; i++)
                {
                    if (_arr[i] == _targetItem)
                    {
                        _index = i;
                        break;
                    }
                }
            }
            else
            {
                for (int i = _arr.Length - 1; 0 <= i; i--)
                {
                    if (_arr[i] == _targetItem)
                    {
                        _index = i;
                        break;
                    }
                }
            }

            return _index;
        }
        #endregion
        #region Casting Group
        // String
        public static T ToEnum<T>(this string value)
        {
            return (T)System.Enum.Parse(typeof(T), value, true);
        }
        public static int ToInt(this string value)
        {
            if (string.IsNullOrEmpty(value) == true) return 0;

            return System.Int32.Parse(value);
        }
        public static float ToFloat(this string value)
        {
            if (string.IsNullOrEmpty(value) == true) return 0f;

            float returnValue = 0f;

            System.Single.TryParse(value, out returnValue);

            return returnValue;
        }
        public static Double ToDouble(this string value)
        {
            if (string.IsNullOrEmpty(value) == true) return 0d;

            double returnValue = 0d;

            System.Double.TryParse(value, out returnValue);

            return returnValue;
        }
        public static bool ToBool(this string value)
        {
            switch (value)
            {
                case "True":
                case "TRUE":
                case "T":
                case "1":
                    return true;

                default:
                    return false;
            }
        }
        public static Byte ToByte(this string value)
        {
            if (string.IsNullOrEmpty(value) == true) return 0;

            Byte returnValue = 0;

            System.Byte.TryParse(value, out returnValue);

            return returnValue;
        }

        // Enum
        public static int ToInt(this System.Enum value)
        {
            // 이거 GC 발생한다고 함

            object _returnValue = Convert.ChangeType(value, typeof(int));
            return (int)_returnValue;
        }

        // Float
        public static int GetPercent_Func(this float _value)
        {
            return (_value * 100f).ToInt();
        }
        public static string GetPercentStr_Func(this float _value, int _pointNumber = 0, bool _isContainPercent = true)
        {
            _value *= 100f;

            if (_isContainPercent == true)
            {
                string _valueArr = _value.ToString_Func(_pointNumber);
                return StringBuilder_C.Append_Func(_valueArr, StringBuilder_C.Percent);
            }
            else
            {
                return _value.ToString_Func(_pointNumber);
            }
        }
        public static int ToInt(this float _value, bool _isRound = false)
        {
            return _isRound == false ? (int)_value : Mathf.RoundToInt(_value);
        }
        public static int ToInt(this double _value)
        {
            return (int)_value;
        }
        private const string format0N0 = "{0:N0}";
        private const string format0N1 = "{0:N1}";
        private const string format0N2 = "{0:N2}";
        private const string format0N3 = "{0:N3}";
        private const string format0N4 = "{0:N4}";
        public static string ToString_Func(this float _value, int _pointNumber = 0)
        {
            if (0 < _pointNumber)
            {
                if (_pointNumber == 1)
                {
                    return string.Format(format0N1, _value);
                }
                else if (_pointNumber == 2)
                {
                    return string.Format(format0N2, _value);
                }
                else if (_pointNumber == 3)
                {
                    return string.Format(format0N3, _value);
                }
                else
                {
                    // 부동소수점의 오차범위
                    // 4자리수 넘어서까지 쓸 일 있으면 추가 바람

                    return string.Format(format0N4, _value);
                }
            }
            else
            {
                return string.Format(format0N0, _value);
            }
        }
        public static string ToString_Second_Func(this float _value)
        {
            string _timeStr = string.Empty;
            if (10f <= _value)
                _timeStr = _value.ToString_Func(0);
            else if (1f <= _value)
                _timeStr = _value.ToString_Func(1);
            else
                _timeStr = _value.ToString_Func(2);

            return _timeStr;
        }

        // Int
        private const string formatN0 = "N0";
        public static string ToString_Func(this int _value)
        {
            return _value.ToString(formatN0);
        }

        private const string FillZeroFormat = "D{0}";
        public static string ToString_Fill_Func(this int _value, int _fillZero)
        {
            string _fillZeroFormat = string.Format(FillZeroFormat, _fillZero);
            return _value.ToString(_fillZeroFormat);
        }
        #endregion
        #region Color Group
        public static Color GetGradientColor_Func(this Color _color, float _value)
        {
            float _rGap = 1f - ((1f - _color.r) * _value);
            float _gGap = 1f - ((1f - _color.g) * _value);
            float _bGap = 1f - ((1f - _color.b) * _value);

            return new Color(_rGap, _gGap, _bGap, 1f);
        }
        #endregion
        #region Dictionary Group
        public static Dictionary<KeyType, ValueType> SetInstance_NoGC_EnumKey_Func<KeyType, ValueType>(IEqualityComparer<KeyType> _iEqualityComparer)
        {
            return new Dictionary<KeyType, ValueType>(_iEqualityComparer);
        }
        /*
         *  예제 코드
        class Test : IEqualityComparer<EnumType>
        {
            public bool Equals(EnumType x, EnumType y)
            {
                return x == y;
            }

            public int GetHashCode(EnumType obj)
            {
                return (int)obj;
            }
        }
        */
        public static void Add_Func<KeyType, ValueType>(this Dictionary<KeyType, ValueType> _dic, KeyType _addKey, ValueType _addValue)
        {
            // 오류 검출용

            if (_dic.ContainsKey(_addKey) == false)
            {
                _dic.Add(_addKey, _addValue);
            }
            else
            {
                Debug_C.Warning_Func("Dictionary에 다음 Key가 이미 존재합니다. : " + _addKey);
            }
        }
        public static void Remove_Func<KeyType, ValueType>(this Dictionary<KeyType, ValueType> _dic, KeyType _removeKey)
        {
            // 오류 검출용

            if (_dic.ContainsKey(_removeKey) == true)
            {
                _dic.Remove(_removeKey);
            }
            else
            {
                Debug_C.Warning_Func("Dictionary에 지우려고 하는 다음 Key가 존재하지 않습니다. : " + _removeKey);
            }
        }
        public static bool TryRemove_Func<KeyType, ValueType>(this Dictionary<KeyType, ValueType> _dic, KeyType _key, out ValueType _value)
        {
            // 오류 검출용

            if (_dic.TryGetValue(_key, out _value) == true)
            {
                _dic.Remove(_key);

                return true;
            }
            else
            {
                return false;
            }
        }

        public static void SetClearToValue_Func<KeyType, ValueType>(this Dictionary<KeyType, ValueType[]> _clearDic, ValueType _clearValue)
        {
            // 함수의 Value 인자로 모두 채우기

            int _keyNum = _clearDic.Keys.Count;
            KeyType[] _keyTypeArr = new KeyType[_keyNum];
            _clearDic.Keys.CopyTo(_keyTypeArr, 0);

            for (int i = 0; i < _keyNum; i++)
            {
                KeyType _keyType = _keyTypeArr[i];

                int _valueNum = _clearDic[_keyType].Length;
                for (int j = 0; j < _valueNum; j++)
                {
                    _clearDic[_keyType][j] = _clearValue;
                }
            }
        }

        public static ValueType GetValue_Func<KeyType, ValueType>(this Dictionary<KeyType, ValueType> _dic, KeyType _key)
        {
            // Key에 해당하는 Value 반환
            // 오류 검출용

            ValueType _returnValue;
            if (_dic.TryGetValue(_key, out _returnValue) == true)
            {
                return _returnValue;
            }
            else
            {
                Debug.LogError("Key 없음 : " + _key);
                return default;
            }
        }
        public static ValueType[] GetValue_Func<KeyType, ValueType>(this Dictionary<KeyType, ValueType> _dic, params KeyType[] _keyArr)
        {
            // Key에 해당하는 Value 반환
            // 오류 검출용
            // 인자 Key 중 중복Key가 있는지 검사하는 기능도 추가하자

            List<ValueType> _list = new List<ValueType>();

            for (int i = 0; i < _keyArr.Length; i++)
            {
                ValueType _value = _dic.GetValue_Func(_keyArr[i]);
                _list.Add(_value);
            }

            return _list.ToArray();
        }
        public static ValueType[] GetValue_Func<KeyType, ValueType>(this Dictionary<KeyType, ValueType> _dic)
        {
            // 딕셔너리의 모든 Value를 배열로 반환

            ValueType[] _returnValueArr = new ValueType[_dic.Values.Count];

            _dic.Values.CopyTo(_returnValueArr, 0);

            return _returnValueArr;
        }
        public static void GetValue_Func<KeyType, ValueType>(this Dictionary<KeyType, ValueType> _dic, Action<ValueType> _del)
        {
            foreach (KeyValuePair<KeyType, ValueType> item in _dic)
            {
                ValueType _value = item.Value;
                if (_value != null) _del(_value);
            }
        }
        public static KeyType[] GetKeys_Func<KeyType, ValueType>(this Dictionary<KeyType, ValueType> _dic)
        {
            // 딕셔너리에 입력된 모든 Key를 반환한다.

            KeyType[] _keyTypeArr = new KeyType[_dic.Keys.Count];
            _dic.Keys.CopyTo(_keyTypeArr, 0);
            return _keyTypeArr;
        }

        public static ValueType ReplaceValue_Func<KeyType, ValueType>(this Dictionary<KeyType, ValueType> _dic, KeyType _key, ValueType _value)
        {
            // 신규 Value를 삽입하고 기존 Value는 Dictionary에서 제거 후 반환한다.

            ValueType _originalValue = _dic.GetValue_Func(_key);
            _dic.Remove(_key);
            _dic.Add(_key, _value);
            return _originalValue;
        }
        #endregion
        #region List Group
        public static void AddNewItem_Func<ValueType>(this List<ValueType> _list, ValueType _addItem)
        {
            bool _isContain = _list.Contains(_addItem);

            if (_isContain == false)
            {

            }
            else
            {
                Debug.LogWarning("이미 삽입되어 있는 Item을 중복해서 삽입하였습니다. : " + _addItem);
            }

            _list.Add(_addItem);
        }
        public static void AddNewItem_Func<ValueType>(this List<ValueType> _list, ValueType[] _addItemArr)
        {
            for (int i = 0; i < _addItemArr.Length; i++)
            {
                _list.AddNewItem_Func(_addItemArr[i]);
            }
        }
        public static void AddNewItem_Func<ValueType>(this List<ValueType> _list, List<ValueType> _addItemList)
        {
            for (int i = 0; i < _addItemList.Count; i++)
            {
                _list.AddNewItem_Func(_addItemList[i]);
            }
        }
        public static void Add_Func<ValueType>(this List<ValueType> _list, ValueType[] _addItemArr)
        {
            for (int i = 0; i < _addItemArr.Length; i++)
            {
                _list.Add(_addItemArr[i]);
            }
        }
        public static void Add_Func<ValueType>(this List<ValueType> _list, List<ValueType> _addItemList)
        {
            for (int i = 0; i < _addItemList.Count; i++)
            {
                _list.Add(_addItemList[i]);
            }
        }

        public static bool InsertNewItem_Func<ValueType>(this List<ValueType> _list, int _id, ValueType _addItem)
        {
            bool _isContain = _list.Contains(_addItem);

            if (_isContain == false)
            {

            }
            else
            {
                Debug.LogWarning("이미 삽입되어 있는 Item을 중복해서 삽입하였습니다. : " + _addItem);
            }

            _list.Insert(_id, _addItem);

            return _isContain;
        }
        public static void Remove_Func<ValueType>(this List<ValueType> _list, ValueType _removeItem)
        {
            if (_list.Contains(_removeItem) == true)
            {
                _list.Remove(_removeItem);
            }
            else
            {
                Debug_C.Warning_Func("존재하지 않는 Item을 Remove하고자 합니다. : " + _removeItem);
            }
        }
        // 이거 밸류 타입도 문제 없는지 확인 필요함
        public static void Swap_Func<T>(this List<T> _arr, int _swapIndex1, int _swapIndex2)
        {
            if (_swapIndex1 != _swapIndex2)
            {
                if (_swapIndex1 < _arr.Count && _swapIndex2 < _arr.Count && 0 <= _swapIndex1 && 0 <= _swapIndex2)
                {
                    T _temp = _arr[_swapIndex1];
                    _arr[_swapIndex1] = _arr[_swapIndex2];
                    _arr[_swapIndex2] = _temp;
                }
                else
                {
                    Debug_C.Error_Func("Swap하려는 배열의 크기는 " + _arr.Count + ". 하지만 접근하려는 Index는 " + _swapIndex1 + ", 그리고 " + _swapIndex2);
                }
            }
        }

        public static ValueType[] GetRandomPick_Func<ValueType>(this List<ValueType> _list, int _pickNum)
        {
            // ToArray()의 퍼포먼스를 고려해서 사용할 것!
            return _list.ToArray().GetRandomPick_Func(_pickNum);
        }
        public static ValueType GetLastItem_Func<ValueType>(this List<ValueType> _list)
        {
            // 리스트의 마지막 아이템 반환

            int _count = _list.Count;
            return 0 < _count ? _list[_count - 1] : default;
        }
        public static ValueType GetHalfItem_Func<ValueType>(this List<ValueType> _list)
        {
            // 리스트의 중간에 배치된 아이템 반환

            int _listNum = _list.Count;

            int _halfID = _listNum / 2;

            ValueType _halfItem = _list[_halfID];

            return _halfItem;
        }
        public static ValueType GetRandItem_Func<ValueType>(this List<ValueType> _list)
        {
            int _cnt = _list.Count;
            int _randValue = UnityEngine.Random.Range(0, _cnt);
            return _list[_randValue];
        }

        public static ValueType GetTakeItem_Func<ValueType>(this List<ValueType> _list, int _index = 0)
        {
            ValueType _item = _list[_index];

            _list.RemoveAt(_index);

            return _item;
        }
        #endregion
        #region Transform Group
        // 2D용
        public static void LookAt_Func(this Transform _thisTrf, Transform _targetTrf)
        {
            _thisTrf.LookAt_Func(_targetTrf.position);
        }
        public static void LookAt_Func(this Transform _thisTrf, Vector2 _targetPos)
        {
            _thisTrf.rotation = Math_C.GetLookAt_Func(_thisTrf.position, _targetPos);
        }
        public static float GetAngle_Func(this Transform _thisTrf, Vector2 _targetPos, bool _isRelativeToRotate = false)
        {
            return _thisTrf.position.GetAngle_Func(_targetPos, _isRelativeToRotate);
        }

        public static void SetPosX_Func(this Transform _thisTrf, float _value, UnityEngine.Space _space)
        {
            if (_space == Space.World)
            {
                _thisTrf.position = new Vector3(_value, _thisTrf.position.y, _thisTrf.position.z);
            }
            else
            {
                _thisTrf.localPosition = new Vector3(_value, _thisTrf.localPosition.y, _thisTrf.localPosition.z);
            }
        }
        public static void SetPosY_Func(this Transform _thisTrf, float _value, UnityEngine.Space _space = Space.World)
        {
            if (_space == Space.World)
            {
                _thisTrf.position = new Vector3(_thisTrf.position.x, _value, _thisTrf.position.z);
            }
            else
            {
                _thisTrf.localPosition = new Vector3(_thisTrf.localPosition.x, _value, _thisTrf.localPosition.z);
            }
        }
        public static void SetPosZ_Func(this Transform _thisTrf, float _value, UnityEngine.Space _space)
        {
            if (_space == Space.World)
            {
                _thisTrf.position = new Vector3(_thisTrf.position.x, _thisTrf.position.y, _value);
            }
            else
            {
                _thisTrf.localPosition = new Vector3(_thisTrf.localPosition.x, _thisTrf.localPosition.y, _value);
            }
        }

        public static void SetRotZ_Func(this Transform _thisTrf, float _value, UnityEngine.Space _space = Space.World)
        {
            if (_space == Space.World)
                _thisTrf.eulerAngles = Vector3.forward * _value;
            else
                _thisTrf.localEulerAngles = Vector3.forward * _value;
        }

        public static void SetScale_Func(this Transform _thisTrf, float _value)
        {
            _thisTrf.localScale = Vector3.one * _value;
        }
        public static void SetScaleUp_Func(this Transform _thisTrf, float _value)
        {
            _thisTrf.localScale = new Vector3(_thisTrf.localScale.x, _value, _thisTrf.localScale.z);
        }

        public static float GetDistance_Func(this Transform _thisTrf, Transform _targetTrf)
        {
            return _thisTrf.GetDistance_Func(_targetTrf.position);
        }
        public static float GetDistance_Func(this Transform _thisTrf, Vector2 _targetPos)
        {
            return Vector2.Distance(_thisTrf.position, _targetPos);
        }

        public static string GetPath_Func(this Transform _trf)
        {
            string _pathStr = _trf.name;

            _trf = _trf.parent;

            while (_trf is null == false)
            {
                _pathStr = StringBuilder_C.Append_Func(_trf.name, " / ", _pathStr);

                _trf = _trf.parent;
            }

            return _pathStr;
        }

        public static void SetParent_Func(this Transform _trf, Transform _parentTrf)
        {
            _trf.SetParent_Func(_parentTrf, Vector3.zero, Vector3.one);
        }
        public static void SetParent_Func(this Transform _trf, Transform _parentTrf, Vector3 _localPos, Vector3 _localScale)
        {
            _trf.SetParent(_parentTrf);
            _trf.localPosition = _localPos;
            _trf.localScale = _localScale;
        }
        #endregion
        #region UGUI Group
        public static void SetFade_Func(this SpriteRenderer _spriteRend, float _alphaValue)
        {
            Color _returnColor = _spriteRend.color;

            _spriteRend.color = GetNaturalAlphaColor_Func(_returnColor, _alphaValue);
        }
        public static void SetFade_Func(this Image _image, float _alphaValue)
        {
            Color _returnColor = _image.color;

            _image.color = GetNaturalAlphaColor_Func(_returnColor, _alphaValue);
        }
        public static void SetFade_Func(this Text _text, float _alphaValue)
        {
            Color _returnColor = _text.color;

            _text.color = GetNaturalAlphaColor_Func(_returnColor, _alphaValue);
        }
        public static void SetFade_Func(this Graphic _graphic, float _alphaValue)
        {
            Color _returnColor = _graphic.color;

            _graphic.color = GetNaturalAlphaColor_Func(_returnColor, _alphaValue);
        }

        public static Color GetNaturalAlphaColor_Func(this Color _color, float _alphaValue)
        {
            Color _returnColor = new Color
                (
                _color.r,
                _color.g,
                _color.b,
                _alphaValue
                );

            return _returnColor;
        }
        public static void SetColorOnBaseAlpha_Func(this Image _image, Color _setColor)
        {
            _setColor = new Color
                (
                _setColor.r,
                _setColor.g,
                _setColor.b,
                _image.color.a
                );

            _image.color = _setColor;
        }
        public static void SetNativeSize_Func(this Image _image, Sprite _sprite)
        {
            _image.sprite = _sprite;
            _image.SetNativeSize();
        }

        public static void FillAmount_Func(this Image _image, int _setValue, int _maxValue)
        {
            _image.FillAmount_Func((float)_setValue, (float)_maxValue);
        }
        public static void FillAmount_Func(this Image _image, float _setValue, int _maxValue)
        {
            _image.FillAmount_Func(_setValue, (float)_maxValue);
        }
        public static void FillAmount_Func(this Image _image, int _setValue, float _maxValue)
        {
            _image.FillAmount_Func((float)_setValue, _maxValue);
        }
        public static void FillAmount_Func(this Image _image, float _setValue, float _maxValue)
        {
            if (0f < _setValue && 0f < _maxValue)
                _image.fillAmount = _setValue / _maxValue;
            else
                _image.fillAmount = 0f;
        }

        public static void SetText_Func(this Text _txt, int _value)
        {
            _txt.text = _value.ToString();
        }
        public static void SetText_Func(this Text _txt, float _value, int _digitNum = -1)
        {
            if (_digitNum == -1)
            {
                _txt.text = _value.ToString();
            }
            else
            {
                _txt.text = _value.ToString_Func(_digitNum);
            }
        }

        public static void SetText_Func(this TMPro.TextMeshProUGUI _tmp, int _value, bool _isContainComma = true)
        {
            _tmp.text = _isContainComma == true ? _value.ToString_Func() : _value.ToString();
        }
        public static void SetText_Func(this TMPro.TextMeshPro _tmp, int _value)
        {
            _tmp.text = _value.ToString();
        }

        public static float GetScrollRtrfHeightDynamic_Func(this GridLayoutGroup _gridLayoutGroup, int _elemNum)
        {
            return GetScrollRtrfHeightDynamic_Func(_gridLayoutGroup, _elemNum, out _);
        }
        public static float GetScrollRtrfHeightDynamic_Func(this GridLayoutGroup _gridLayoutGroup, int _elemNum, out int _rowNum)
        {
            // _gridLayoutGroup x 피벗이 0.5이어야 함

            float _scrollWidthSize = _gridLayoutGroup.transform.localPosition.x * 2f;
            float _elemWidth = _gridLayoutGroup.cellSize.x + _gridLayoutGroup.spacing.x;
            int _columnNum = (int)(_scrollWidthSize / _elemWidth);
            float _elemHeight = _gridLayoutGroup.cellSize.y + _gridLayoutGroup.spacing.y;

            return GetScrollRtrfHeightDynamic_Func(_elemNum, _columnNum, _elemHeight, out _rowNum);
        }
        public static float GetScrollRtrfHeightDynamic_Func(int _elemNum, int _columnNum, float _elemHeight, out int _rowNum)
        {
            if (1 <= _columnNum)
            {
                _rowNum = _elemNum / _columnNum;
                if (_elemNum % _columnNum != 0)
                    ++_rowNum;
            }
            else
            {
                _rowNum = _elemNum;
            }

            return _elemHeight * _rowNum;
        }

        public static void SetScrollRtrfHeightDynamic_Func(this RectTransform _scrollRtrf,
            int _elemNum, GridLayoutGroup _gridLayoutGroup, out float _scrollContentSizeY, float _moreSpaceY = 0f)
        {
            _scrollContentSizeY = _gridLayoutGroup.GetScrollRtrfHeightDynamic_Func(_elemNum, out _);

            SetHeight_Func(_scrollRtrf, _scrollContentSizeY + _moreSpaceY);
        }
        public static void SetScrollRtrfHeightDynamic_Func(this RectTransform _scrollRtrf,
            int _elemNum, GridLayoutGroup _gridLayoutGroup, out int _rowNum, out float _scrollContentSizeY, float _moreSpaceY = 0f)
        {
            _scrollContentSizeY = _gridLayoutGroup.GetScrollRtrfHeightDynamic_Func(_elemNum, out _rowNum);

            SetHeight_Func(_scrollRtrf, _scrollContentSizeY + _moreSpaceY);
        }
        public static void SetScrollRtrfHeightDynamic_Func(this RectTransform _scrollRtrf,
            int _elemNum, float _scrollWidthSize, float _elemWidth, float _elemHeight, out int _rowNum, out float _scrollContentSizeY, float _moreSpaceY = 0f)
        {
            int _columnNum = (int)(_scrollWidthSize / _elemWidth);
            SetScrollRtrfHeightDynamic_Func(_scrollRtrf, _elemNum, _elemHeight, _moreSpaceY, _columnNum, out _rowNum, out _scrollContentSizeY);
        }
        public static void SetScrollRtrfHeightDynamic_Func(this RectTransform _scrollRtrf,
            int _elemNum, float _elemHeight, VerticalLayoutGroup _verticalLayoutGroup)
        {
            float _moreSpaceY = _verticalLayoutGroup.padding.top + _verticalLayoutGroup.padding.bottom;
            _moreSpaceY += _verticalLayoutGroup.spacing * (_elemNum - 1);
            SetScrollRtrfHeightDynamic_Func(_scrollRtrf, _elemNum, _elemHeight, _moreSpaceY, 1, out _, out _);
        }
        public static void SetScrollRtrfHeightDynamic_Func(this RectTransform _scrollRtrf,
            int _elemNum, float _elemHeight, float _moreSpaceY, int _columnNum, out int _rowNum, out float _scrollContentSizeY)
        {
            _scrollContentSizeY = GetScrollRtrfHeightDynamic_Func(_elemNum, _columnNum, _elemHeight, out _rowNum);
            SetHeight_Func(_scrollRtrf, _scrollContentSizeY + _moreSpaceY);
        }

        public static void SetHeight_Func(this RectTransform _scrollRtrf, float _height)
        {
            // Rtrf의 앵커가 아래와 같이 세팅돼야 함
            // _scrollRtrf.anchorMin = Vector2.up;
            // _scrollRtrf.anchorMax = Vector2.one;

            _scrollRtrf.anchorMin = Vector2.up;
            _scrollRtrf.anchorMax = Vector2.one;
            _scrollRtrf.sizeDelta = Vector2.up * _height;
        }
        #endregion
        #region Data Structure
        public static bool HasItem_Func<T>(this Queue<T> _queue)
        {
            return 0 < _queue.Count ? true : false;
        }
        public static bool TryDequeue_Func<T>(this Queue<T> _queue, out T _tryGet)
        {
            bool _isHave = false;

            if (0 < _queue.Count)
            {
                _isHave = true;

                _tryGet = _queue.Dequeue();
            }
            else
            {
                _isHave = false;

                _tryGet = default(T);
            }

            return _isHave;
        }
        #endregion
        #region DateTime
        public static DateTime GetMidNight_Func(this DateTime _dateTime)
        {
            _dateTime = _dateTime.AddDays(1);
            return new DateTime(_dateTime.Year, _dateTime.Month, _dateTime.Day, 0, 0, 0);
        }
        public static DateTime GetNextDayOfWeek_Func(this DateTime _dateTime, DayOfWeek _targetDayOfWeek)
        {
            DayOfWeek _nowDayOfWeek = _dateTime.DayOfWeek;

            int _nextDay = -1;
            _nextDay = _targetDayOfWeek - _nowDayOfWeek;
            if (_targetDayOfWeek <= _nowDayOfWeek)
                _nextDay += 7;

            _dateTime = _dateTime.AddDays(_nextDay);
            return new DateTime(_dateTime.Year, _dateTime.Month, _dateTime.Day, 0, 0, 0);
        }
        public static DateTime GetNextBeginningOfMonth_Func(this DateTime _dateTime)
        {
            _dateTime = _dateTime.AddMonths(1);
            return new DateTime(_dateTime.Year, _dateTime.Month, 1, 0, 0, 0);
        }
        public static DateTime GetAfterDay_Func(this DateTime _dateTime, int _days, bool _isMidnight = false)
        {
            DateTime _afterTime = _dateTime.AddDays(_days);
            return _isMidnight == false ? _afterTime : _afterTime.GetMidNight_Func();
        }
        public static double GetTotalSec_Func(this DateTime _dateTime)
        {
            TimeSpan _timeSpan = new TimeSpan(_dateTime.Ticks);
            return _timeSpan.TotalSeconds;
        }
        public static double GetTotalMin_Func(this DateTime _dateTime)
        {
            TimeSpan _timeSpan = new TimeSpan(_dateTime.Ticks);
            return _timeSpan.TotalMinutes;
        }
        public static TimeSpan GetTimeSpan_Func(this DateTime _dateTime)
        {
            return new TimeSpan(_dateTime.Ticks);
        }

        // TimeSpan
        public static string ToString_HMS_Func(this TimeSpan _value, bool _isIncludeHour = true, bool _isIncludeMilliSecond = false)
        {
            DateTime _dateTime = _value.ToDateTime_Func();
            return _dateTime.ToString_HMS_Func();
        }
        public static DateTime ToDateTime_Func(this TimeSpan _value)
        {
            return new DateTime(_value.Ticks);
        }

        // Common
        public static string ToString_HMS_Func(this DateTime _value, bool _isIncludeMilliSecond = false, bool _isIncludeHour = true)
        {
            return ToString_Time_Func(_value, false, _isIncludeMilliSecond, _isIncludeHour, default);
        }
        public static string ToString_MS_Func(this DateTime _value, bool _isIncludeMilliSecond = false, bool _isIncludeSecondToMinute = false)
        {
            return ToString_Time_Func(_value, true, _isIncludeMilliSecond, default, _isIncludeSecondToMinute);
        }

        private const string formatHMSms = "{0:00}:{1:00}:{2:00}.{3:00}";
        private const string formatMSms = "{0:00}:{1:00}.{2:00}";
        private const string formatHMS = "{0:00}:{1:00}:{2:00}";
        private const string formatMS = "{0:00}:{1:00}";
        private const string formatSms = "{0:00}.{1:00}";
        private const string formatS = "{0:00}";
        private static string ToString_Time_Func(DateTime _value, bool _isIncludeMinuteToHour, bool _isIncludeMilliSecond, bool _isIncludeHour, bool _isIncludeSecondToMinute)
        {
            int _hour = _value.Hour;
            int _minute = _value.Minute;
            int _second = _value.Second;
            int _miSec = _value.Millisecond;

            // 시를 분에 포함 안 하나?
            if (_isIncludeMinuteToHour == false)
            {
                // 밀리초를 포함 안 하나?
                if (_isIncludeMilliSecond == false)
                {
                    // 시를 포함하나?
                    if (_isIncludeHour == true)
                        return string.Format(formatHMS, _hour, _minute, _second);
                    else
                        return string.Format(formatMS, _minute, _second);
                }
                else
                {
                    // 시를 포함하나?
                    if (_isIncludeHour == true)
                        return string.Format(formatHMSms, _hour, _minute, _second, _miSec);
                    else
                        return string.Format(formatMSms, _minute, _second, _miSec);
                }
            }

            // 시를 분에 포함하나?
            else
            {
                _minute += _hour * 60;

                // 분을 초에 포함 안 하나?
                if (_isIncludeSecondToMinute == false)
                {
                    // 밀리초를 포함 안 하나?
                    if (_isIncludeMilliSecond == false)
                    {
                        return string.Format(formatMS, _minute, _second);
                    }
                    else
                    {
                        return string.Format(formatMSms, _minute, _second, _miSec);
                    }
                }

                // 분을 초에 포함하나?
                else
                {
                    _second += _minute * 60;

                    // 밀리초를 포함 안 하나?
                    if (_isIncludeMilliSecond == false)
                    {
                        return string.Format(formatS, _second);
                    }
                    else
                    {
                        return string.Format(formatSms, _second, _miSec);
                    }
                }
            }
        }
        #endregion
        #region StringBuilder
        public static void RemoveAll_Func(this StringBuilder _stringBuilder)
        {
            _stringBuilder.Remove(0, _stringBuilder.Length);
        }
        #endregion
        #region String
        public static bool IsNullOrWhiteSpace_Func(this string _value)
        {
            return string.IsNullOrWhiteSpace(_value);
        }
        public static bool IsCompare_Func(this string _value, string _targetStr)
        {
            return string.Compare(_value, _targetStr) == 0;
        }
        #endregion
        #region Vector
        public static bool CheckInside_Func(this Vector2 _targetPos, Vector2 _spaceMinPos, Vector2 _spaceMaxPos)
        {
            if (_spaceMinPos.x <= _targetPos.x && _targetPos.x <= _spaceMaxPos.x
            && _spaceMinPos.y <= _targetPos.y && _targetPos.y <= _spaceMaxPos.y)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool CheckInside_Func(this Vector2 _targetPos, Transform _spaceMinTrf, Transform _spaceMaxTrf)
        {
            return CheckInside_Func(_targetPos, _spaceMinTrf.position, _spaceMaxTrf.position);
        }

        public static bool CheckClose_Func(this Vector2 _thisPos, Transform _targetTrf, float _distance)
        {
            return _thisPos.CheckClose_Func(_targetTrf.position, _distance);
        }
        public static bool CheckClose_Func(this Vector2 _thisPos, Vector2 _targetPos, float _distance)
        {
            return Vector2.Distance(_thisPos, _targetPos) <= _distance;
        }

        public static float GetAngle_Func(this Vector3 _thisPos, Vector3 _targetPos, bool _isRelativeToRotate = false)
        {
            return Math_C.GetAngle_Func(_thisPos, _targetPos, _isRelativeToRotate);
        }
        public static float GetAngle_Func(this Vector2 _thisPos, Vector2 _targetPos, bool _isRelativeToRotate = false)
        {
            return Math_C.GetAngle_Func(_thisPos, _targetPos, _isRelativeToRotate);
        }
        public static Vector2 GetRight_Func(this Vector2 _thisPos, float _value)
        {
            return new Vector2(_thisPos.x + _value, _thisPos.y);
        }
        public static Vector2 GetUp_Func(this Vector2 _thisPos, float _value)
        {
            return new Vector2(_thisPos.x, _thisPos.y + _value);
        }

        public static Vector2 GetRandX_Func(this Vector2 _thisPos, float _minX, float _maxX)
        {
            return new Vector2(UnityEngine.Random.Range(_minX, _maxX), _thisPos.y);
        }
        public static Vector2 GetRandY_Func(this Vector2 _thisPos, float _minY, float _maxY)
        {
            return new Vector2(_thisPos.x, UnityEngine.Random.Range(_minY, _maxY));
        }
        public static Vector2 GetRand_Func(this Vector2 _thisPos, float _minX, float _maxX, float _minY, float _maxY)
        {
            return new Vector2(UnityEngine.Random.Range(_minX, _maxX), UnityEngine.Random.Range(_minY, _maxY));
        }
        public static Vector2 GetRand_Func(this Vector2 _thisPos)
        {
            if (_thisPos != Vector2.zero)
            {
                float _randX = UnityEngine.Random.Range(0f, _thisPos.x);
                float _randY = UnityEngine.Random.Range(0f, _thisPos.y);
                return new Vector2(_randX, _randY);
            }
            else
            {
                return Vector2.zero;
            }
        }

        public static Vector2 GetCircumferencePos_Func(this Vector3 _circleCenterPos, float _radius, float _angle)
        {
            return Math_C.GetCircumferencePos_Func(_circleCenterPos, _radius, _angle);
        }
        public static Vector2 GetCircumferencePos_Func(this Vector2 _circleCenterPos, float _radius, float _angle)
        {
            return Math_C.GetCircumferencePos_Func(_circleCenterPos, _radius, _angle);
        }
        public static Vector2 GetCircumferencePos_Func(this Vector2 _circleCenterPos, float _radius)
        {
            float _angle = UnityEngine.Random.Range(0f, 360f);
            return GetCircumferencePos_Func(_circleCenterPos, _radius, _angle);
        }

        public static float GetDistance_Func(this Vector3 _thisPos, Vector3 _targetPos)
        {
            return Vector3.Distance(_thisPos, _targetPos);
        }
        public static float GetDistance_Func(this Vector2 _thisPos, Vector2 _targetPos)
        {
            return Vector2.Distance(_thisPos, _targetPos);
        }
        public static bool CheckDistance_Func(this Vector3 _thisPos, Vector3 _targetPos, float _innerDist)
        {
            return Math_C.CheckDistance_Func(ref _thisPos, ref _targetPos, ref _innerDist);
        }
        public static bool CheckDistance_Func(this Vector2 _thisPos, Vector2 _targetPos, float _innerDist)
        {
            return Math_C.CheckDistance_Func(ref _thisPos, ref _targetPos, ref _innerDist);
        }
        #endregion
    }

    // Utility Class
    #region Singleton
    namespace Singleton
    {
        public abstract class Singleton_C<T> : MonoBehaviour where T : MonoBehaviour
        {
            private static T instance;
            public static T Instance
            {
                get
                {
                    if (instance is null)
                        Generate_Func();

                    return instance;
                }
            }

            // 싱글턴을 사용하기 위해 아래의 함수를 최초 1회 호출해야 함.
            // 미호출 시 Property를 통해 예외처리하므로 문제는 없으나 Warning Log는 출력됨.

            public static void Generate_Func()
            {
                T _singletonComponent = FindObjectOfType<T>();

                Singleton_C<T>.Generate_Func(_singletonComponent);
            }
            public static void Generate_Func(T _existenceComonent)
            {
                GameObject _singletonObj = null;

                if (_existenceComonent != null)
                {
                    _singletonObj = _existenceComonent.gameObject;

                    instance = _existenceComonent;
                }
                else
                {
                    Debug.LogWarning("싱글턴 객체를 동적 생성하였습니다. 따라서 Data Initialize를 고려해주시기 바랍니다. - " + typeof(T));

                    _singletonObj = new GameObject();
                    instance = _singletonObj.AddComponent<T>();
                    _singletonObj.name = StringBuilder_C.Append_Func("(Singleton)", typeof(T).ToString());
                }

                UnityEngine.Object.DontDestroyOnLoad(_singletonObj);
            }
        }

        public abstract class Singleton_Cor<T> : Singleton_C<T> where T : MonoBehaviour
        {
            public abstract IEnumerator Init_Cor();
        }
        public abstract class Singleton_Func<T> : Singleton_C<T> where T : MonoBehaviour
        {
            public abstract void Init_Func();
        }
    }
    #endregion
    #region Observer
    namespace Observer
    {
        // 구독자 관리 클래스
        [System.Serializable]
        public class Observer_Manager<SubscriberType>
        {
            [SerializeField] protected List<SubscriberType> subscriberList;

            public Observer_Manager()
            {
                subscriberList = new List<SubscriberType>();
            }

            // 구독
            public bool Subscribe_Func(SubscriberType _subscriber, int _insertID = -1, bool _isEnableOverlap = false)
            {
                bool _isContainListener = subscriberList.Contains(_subscriber);

                bool _isAddable = true;
                if (_isEnableOverlap == false)
                {
                    if (_isContainListener == false)
                    {

                    }
                    else
                    {
                        _isAddable = false;

                        Debug_C.Warning_Func("중복 구독 : " + _subscriber);
                    }
                }
                else
                {

                }

                if (_isAddable == true)
                {
                    if (_insertID == -1)
                        subscriberList.Add(_subscriber);
                    else
                        subscriberList.Insert(_insertID, _subscriber);
                }

                return _isContainListener;
            }

            // 구독 전체 해지
            public bool UnsubscribeAll_Func()
            {
                // 구독 전체 해제
                // 구독자가 있는가?

                if (0 < subscriberList.Count)
                {
                    subscriberList.Clear();

                    return true;
                }
                else
                {
                    return false;
                }
            }

            // 특정 구독자만 해지
            public bool Unsubscribe_Func(SubscriberType _subscriber)
            {
                if (0 < this.subscriberList.Count && this.subscriberList.Contains(_subscriber) == true)
                {
                    this.subscriberList.Remove(_subscriber);

                    return true;
                }
                else
                {
                    Debug_C.Warning_Func("해지할 대상이 애초에 구독하고 있지 않음 : " + _subscriber);

                    return false;
                }
            }

            // 특정 구독자의 구독 여부
            public bool IsSubscribed_Func(SubscriberType _subscriber)
            {
                if (this.subscriberList.Count == 0)
                    return false;

                return this.subscriberList.Contains(_subscriber);
            }

            // 구독자 숫자
            public int GetSubscriberNum_Func()
            {
                return this.subscriberList.Count;
            }

            public bool HasSubscriber { get { return 0 < this.subscriberList.Count ? true : false; } }

            // 모든 구독자에게 접근
            public void AccessWholeSubscriber_Func(Action<SubscriberType> _del)
            {
                foreach (SubscriberType _subscriber in this.subscriberList)
                    _del(_subscriber);
            }
        }
        #region Action 0
        public class Observer_Action : Observer_Manager<Action>
        {
            public bool Notify_Func()
            {
                // 등록된 모든 구독자에게 알림
                // 구독자가 있는지 확인

                if (0 < subscriberList.Count)
                {
                    for (int i = subscriberList.Count - 1; 0 <= i; --i)
                    {
                        subscriberList[i]();
                    }

                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        #endregion
        #region Action 1
        [System.Serializable]
        public class Observer_Action<T> : Observer_Manager<Action<T>>
        {
            public bool Notify_Func(T _t)
            {
                // 등록된 모든 구독자에게 알림
                // 구독자가 있는지 확인

                if (0 < subscriberList.Count)
                {
                    for (int i = subscriberList.Count - 1; 0 <= i; --i)
                    {
                        subscriberList[i](_t);
                    }

                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        #endregion
        #region Action 2
        [System.Serializable]
        public class Observer_Action<T1, T2> : Observer_Manager<Action<T1, T2>>
        {
            public bool Notify_Func(T1 _t1, T2 _t2)
            {
                // 등록된 모든 구독자에게 알림
                // 구독자가 있는지 확인

                if (0 < subscriberList.Count)
                {
                    for (int i = subscriberList.Count - 1; 0 <= i; --i)
                    {
                        subscriberList[i](_t1, _t2);
                    }

                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        #endregion
        #region Action 3
        [System.Serializable]
        public class Observer_Action<T1, T2, T3> : Observer_Manager<Action<T1, T2, T3>>
        {
            public bool Notify_Func(T1 _t1, T2 _t2, T3 _t3)
            {
                // 등록된 모든 구독자에게 알림
                // 구독자가 있는지 확인

                if (0 < subscriberList.Count)
                {
                    for (int i = subscriberList.Count - 1; 0 <= i; --i)
                    {
                        subscriberList[i](_t1, _t2, _t3);
                    }

                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        #endregion
    }
    #endregion
    #region Which One
    namespace WhichOne
    {
        public sealed class WhichOne<T> where T : class, IWhichOne
        {
            [SerializeField] private T whichOne;

            public WhichOne()
            {
                whichOne = null;
            }
            public void Selected_Func(T _whichOne, bool _isCancelWhenTwiceSelected = true)
            {
                // 인자값을 선택 개체로 등록하고 '선택'이벤트 전달.
                // 만약 기 개체를 선택한 경우 선택 개체에게 중복 선택임을 알림
                // 만약 이미 선택 개체가 있다면, 기 선택 개체에게 '선택 해제'이벤트 전달

                if (this.whichOne is null)
                {
                    this.whichOne = _whichOne;

                    _whichOne.Selected_Func();
                }
                else
                {
                    if (this.whichOne == _whichOne)
                    {
                        _whichOne.Selected_Func(true);

                        if (_isCancelWhenTwiceSelected == true)
                            this.ClearWhichOne_Func();
                    }
                    else
                    {
                        this.whichOne.SelectCancel_Func();

                        this.whichOne = _whichOne;

                        _whichOne.Selected_Func();
                    }
                }
            }
            public void SelectCancel_Func()
            {
                // 선택 해제. 선택 개체에게 선택 해제 이벤트 알림

                if (this.whichOne is null == false)
                {
                    this.whichOne.SelectCancel_Func();

                    this.whichOne = null;
                }
            }
            public T GetWhichOne_Func()
            {
                // 선택 개체 반환

                return this.whichOne;
            }
            public void SetWhichOne_Func(T _iWhichOne)
            {
                this.whichOne = _iWhichOne;
            }
            public bool Compare_Func(T _check)
            {
                // 인자값과 선택 개체가 동일한가?

                return this.whichOne == _check;
            }
            public bool HasWhichOne_Func()
            {
                // 선택한 개체가 있는가?

                return this.whichOne is null == false;
            }
            public void ClearWhichOne_Func()
            {
                this.whichOne = null;
            }
        }

        public interface IWhichOne
        {
            void Selected_Func(bool _repeat = false); // 선택됨
            void SelectCancel_Func(); // 선택 해제됨
        }

        // 1. 선택 순서를 기록하고 이를 역행하면서 선택 해제하고 싶다면?
        /*
         * List를 써서 순서 기록
         * 순서를 역순으로 돌아갈 수 있음
         * List Clear 시점은 Select 값이 없을 때?
         */

        // 2. 선택 개수를 2개 이상인 경우엔?
        /*
         * 선택 개수를 초과할 경우 가장 먼저 선택된 객체가 해제?
         */
    }
    #endregion
    #region Tile System
    namespace TileSystem
    {
        #region TileSystem
        // 사용법
        // 타일을 통제할 매니저는 TileSystem_Class를 상속 받아야 한다.
        // T에는 한 타일에 올라올 수 있는 Type이다.

        // 타일 매니저에 관리 당할 일반 타일 클래스는 Tile_Class를 상속 받아야 한다.
        // 타일 매니저와 일반 타일 클래스는 모두 초기화 함수(Init_Func)을 호출해야 한다.
        // 타일 클래스는 타일 매니저에게 관리를 받기 위해선 SetTile 함수를 호출해야 한다.

        // T는 Tile Type Enum
        public class TileSystem
        {
            protected TileGroup_Class[,] tileGroupClassArr;
            public int FieldSizeX_Max { get { return tileGroupClassArr.GetLength(0); } }
            public int FieldSizeY_Max { get { return tileGroupClassArr.GetLength(1); } }
            public TilePosData FieldSize_Max { get { return new TilePosData(FieldSizeX_Max, FieldSizeY_Max); } }

            // 타일맵 시스템의 시작 WorldSpace위치 보정값
            public virtual Vector2 TilePos_InitData { get { return new Vector2(0, 0); } }

            // 각 타일간의 WorldSpace 간격
            public virtual Vector2 TileSpace { get { return new Vector2(1, 1); } }

            // 초기화
            public TileSystem(int _x, int _y)
            {
                tileGroupClassArr = new TileGroup_Class[_x, _y];

                for (int x = 0; x < _x; x++)
                {
                    for (int y = 0; y < _y; y++)
                    {
                        tileGroupClassArr[x, y] = new TileGroup_Class(x, y);
                    }
                }
            }

            // 특정 타일을 매니저에게 알려서 관리 받도록 세팅
            public void SetTile_Func(Tile_Class _tileClass)
            {
                int _x = _tileClass.GetTilePosX;
                int _y = _tileClass.GetTilePosY;

                if (CheckInOfRange_Func(_x, _y) == true)
                {
                    this.tileGroupClassArr[_x, _y].SetTile_Func(_tileClass);
                }
                else
                {
                    Debug_C.Warning_Func("필드의 영역을 벗어났습니다.");
                }
            }
            public void RemoveTile_Func(Tile_Class _tileClass, bool _isCallTile)
            {
                int _x = _tileClass.GetTilePosX;
                int _y = _tileClass.GetTilePosY;

                if (CheckInOfRange_Func(_x, _y) == true)
                {
                    this.tileGroupClassArr[_x, _y].RemoveTile_Func(_tileClass, _isCallTile);
                }
                else
                {
                    Debug_C.Warning_Func("필드의 영역을 벗어났습니다.");
                }
            }

            // 타일 범위 안에 있는가?
            public bool CheckInOfRange_Func(TilePosData _posData)
            {
                return CheckInOfRange_Func(_posData.X, _posData.Y);
            }
            public bool CheckInOfRange_Func(int _x, int _y)
            {
                if (this.FieldSizeX_Max <= _x || _x < 0)
                {
                    Debug_C.Log_Func("Out of tile randge to X : " + _x);
                    return false;
                }
                else if (this.FieldSizeY_Max <= _y || _y < 0)
                {
                    Debug_C.Log_Func("Out of tile randge to Y : " + _y);
                    return false;
                }
                else
                {
                    return true;
                }
            }
            public bool GetTile_Func(TilePosData _data, string _type, out Tile_Class _tileClass)
            {
                return this.GetTile_Func(_data.X, _data.Y, _type, out _tileClass);
            }
            public bool GetTile_Func(int _x, int _y, string _type, out Tile_Class _tileClass)
            {
                if (CheckInOfRange_Func(_x, _y) == true)
                {
                    TileGroup_Class _tileGroupClass = this.tileGroupClassArr[_x, _y];

                    return _tileGroupClass.TryGetTile_Func(_type, out _tileClass);
                }
                else
                {
                    Debug_C.Warning_Func("필드의 영역을 벗어났습니다.");

                    _tileClass = null;

                    return false;
                }
            }
            public bool[] GetTileArr_Func(TilePosData _posData, out Tile_Class[] _tileClassArr, params string[] _typeArr)
            {
                return GetTileArr_Func(_posData.X, _posData.Y, out _tileClassArr, _typeArr);
            }
            public bool[] GetTileArr_Func(int _x, int _y, out Tile_Class[] _tileClassArr, params string[] _typeArr)
            {
                TileGroup_Class _tileGroupClass = this.tileGroupClassArr[_x, _y];

                bool[] _isReturnArr = new bool[_typeArr.Length];
                _tileClassArr = new Tile_Class[_typeArr.Length];

                for (int i = 0; i < _typeArr.Length; i++)
                {
                    _isReturnArr[i] = _tileGroupClass.TryGetTile_Func(_typeArr[i], out _tileClassArr[i]);
                }

                return _isReturnArr;
            }
            public bool TryGetTile_Func<TileClass>(TilePosData _tilePosData, DirectionType _checkDir, string _getTileGroupType, out TileClass _getTileClass
                , Func<TileClass, bool> _conditionDel = null) where TileClass : Tile_Class
            {
                return this.TryGetTile_Func(_tilePosData.X, _tilePosData.Y, _checkDir, _getTileGroupType, out _getTileClass, _conditionDel);
            }
            public bool TryGetTile_Func<TileClass>(int _x, int _y, DirectionType _checkDir, string _getTileGroupType, out TileClass _getTileClass
                , Func<TileClass, bool> _conditionDel = null) where TileClass : Tile_Class
            {
                // _x, _y지점을 시작으로 _moveDir방향으로 _tileType을 (지정되지 않으면 아무거나) _getTile을 반환한다.

                bool _isOutOfRange = true;
                switch (_checkDir)
                {
                    case DirectionType.Left:
                        if (0 < _x)
                        {
                            _isOutOfRange = false;

                            _x--;
                        }
                        break;

                    case DirectionType.Down:
                        if (0 < _y)
                        {
                            _isOutOfRange = false;

                            _y--;
                        }
                        break;

                    case DirectionType.Up:
                        if (_y < this.FieldSizeY_Max - 1)
                        {
                            _isOutOfRange = false;

                            _y++;
                        }
                        break;

                    case DirectionType.Right:
                        if (_x < this.FieldSizeX_Max - 1)
                        {
                            _isOutOfRange = false;

                            _x++;
                        }
                        break;

                    default:
                        break;
                }

                if (_isOutOfRange == false)
                {
                    TileGroup_Class _tileGroupClass = this.tileGroupClassArr[_x, _y];
                    if (_tileGroupClass.TryGetTile_Func(_getTileGroupType, out _getTileClass) == true)
                    {
                        if (_conditionDel == null) return true;
                        if (_conditionDel(_getTileClass) == true) return true;
                    }

                    return this.TryGetTile_Func(_x, _y, _checkDir, _getTileGroupType, out _getTileClass, _conditionDel);
                }
                else
                {
                    _getTileClass = default;

                    return false;
                }
            }

            public bool Move_Func(Tile_Class _moveTileClass, int _arrivePosX, int _arrivePosY, DirectionType _moveDir = DirectionType.None, bool _isJustCheck = false
                , params string[] _checkTileTypeArr)
            {
                // 이동 좌표가 타일 범위를 초과했는가?
                bool _isTileRange = this.CheckInOfRange_Func(_arrivePosX, _arrivePosY);

                if (_isTileRange == true)
                {
                    // 이동 타일의 정보
                    int _movePosX = _moveTileClass.GetTilePosX;
                    int _movePosY = _moveTileClass.GetTilePosY;

                    // 도착 타일의 정보
                    TileGroup_Class _arriveTileGroupClass = this.tileGroupClassArr[_arrivePosX, _arrivePosY];
                    Tile_Class[] _arriveTileClassArr = new Tile_Class[_checkTileTypeArr.Length];

                    bool _isMovable = false;
                    bool[] _isArriveTileHaveArr = new bool[_checkTileTypeArr.Length];

                    for (int i = 0; i < _checkTileTypeArr.Length; i++)
                    {
                        _isArriveTileHaveArr[i] = _arriveTileGroupClass.TryGetTile_Func(_checkTileTypeArr[i], out _arriveTileClassArr[i]);

                        // 도착 지점에 특정 타일이 있는가?
                        if (_isArriveTileHaveArr[i] == true)
                        {
                            // 도착 지점의 특정 타일의 이동 가능 여부 확인
                            _isMovable = _arriveTileClassArr[i].CheckMovable_Func(_moveTileClass);

                            if (_isMovable == true)
                            {

                            }
                            else
                            {
                                break;
                            }
                        }
                        else
                        {
                            _isMovable = true;
                        }
                    }

                    // 단순히 확인만 할 것인가?
                    if (_isJustCheck == false)
                    {
                        // 도착 지점으로 이동 가능한가?
                        if (_isMovable == true)
                        {
                            // 타일 그룹에서 이동 타일을 제외시킴
                            TileGroup_Class _moveTileGroupClass = this.tileGroupClassArr[_movePosX, _movePosY];
                            _moveTileGroupClass.RemoveTile_Func(_moveTileClass, false);

                            for (int i = 0; i < _isArriveTileHaveArr.Length; i++)
                            {
                                // 도착 지점에 특정 타일이 있는가?
                                if (_isArriveTileHaveArr[i] == true)
                                {
                                    // 도착 지점의 특정 타일에게 '밀려남'을 알림
                                    _arriveTileClassArr[i].Pushed_Func(_moveTileClass, _moveDir);
                                }
                                else
                                {

                                }
                            }

                            _arriveTileGroupClass.SetTile_Func(_moveTileClass);
                        }
                        else
                        {

                        }
                    }
                    else
                    {

                    }

                    return _isMovable;
                }
                else
                {
                    return false;
                }
            }
            public TileGroup_Class GetTileGroupClass_Func(TilePosData _posData)
            {
                return this.GetTileGroupClass_Func(_posData.X, _posData.Y);
            }
            public TileGroup_Class GetTileGroupClass_Func(int _x, int _y)
            {
                this.CheckInOfRange_Func(_x, _y);

                return this.tileGroupClassArr[_x, _y];
            }
            public void OnClear_Func()
            {
                for (int _x = 0; _x < this.FieldSizeX_Max; _x++)
                {
                    for (int _y = 0; _y < this.FieldSizeY_Max; _y++)
                    {
                        var _tileGroupClass = this.tileGroupClassArr[_x, _y];
                        _tileGroupClass.OnClear_Func();
                    }
                }
            }

            public static void GetDirType_Func(TilePosData _xPosData, TilePosData _yPosData, out DirectionType _dirTypeSide, out DirectionType _dirTypeExid)
            {
                if (_xPosData.X < _yPosData.X)
                {
                    _dirTypeSide = DirectionType.Right;
                }
                else if (_xPosData.X > _yPosData.X)
                {
                    _dirTypeSide = DirectionType.Left;
                }
                else
                {
                    _dirTypeSide = DirectionType.None;
                }

                if (_xPosData.Y < _yPosData.Y)
                {
                    _dirTypeExid = DirectionType.Up;
                }
                else if (_xPosData.Y > _yPosData.Y)
                {
                    _dirTypeExid = DirectionType.Down;
                }
                else
                {
                    _dirTypeExid = DirectionType.None;
                }
            }
            public static bool TryGetDirType_Func(TilePosData _xPosData, TilePosData _yPosData, WayType _wayType, out DirectionType _dirType)
            {
                if (_wayType == WayType.Side)
                {
                    if (_xPosData.X < _yPosData.X)
                    {
                        _dirType = DirectionType.Right;
                    }
                    else if (_xPosData.X > _yPosData.X)
                    {
                        _dirType = DirectionType.Left;
                    }
                    else
                    {
                        _dirType = DirectionType.None;
                    }

                    return _dirType != DirectionType.None;
                }
                else if (_wayType == WayType.Exid)
                {
                    if (_xPosData.Y < _yPosData.Y)
                    {
                        _dirType = DirectionType.Up;
                    }
                    else if (_xPosData.Y > _yPosData.Y)
                    {
                        _dirType = DirectionType.Down;
                    }
                    else
                    {
                        _dirType = DirectionType.None;
                    }

                    return _dirType != DirectionType.None;
                }
                else
                {
                    _dirType = default;

                    Debug_C.Error_Func("_wayType : " + _wayType);

                    return false;
                }
            }
            public static int GetDistance_Func(TilePosData _xPosData, TilePosData _yPosData, WayType _wayType)
            {
                int _x = 0;
                int _y = 0;

                if (_wayType == WayType.Side)
                {
                    _x = _xPosData.X;
                    _y = _yPosData.X;
                }
                else if (_wayType == WayType.Exid)
                {
                    _x = _xPosData.Y;
                    _y = _yPosData.Y;
                }
                else
                {
                    Debug_C.Error_Func("_wayType : " + _wayType);
                }

                if (_x < _y)
                {
                    return _y - _x;
                }
                else if (_x > _y)
                {
                    return _x - _y;
                }
                else
                {
                    return 0;
                }
            }
        }
        #endregion
        #region TileGroup_Class
        public class TileGroup_Class
        {
            private int xAxis;
            private int yAxis;

            public int XAxis { get { return xAxis; } }
            public int YAxis { get { return yAxis; } }

            private Dictionary<string, Tile_Class> tileGroupDic;

            public TileGroup_Class(int _x, int _y)
            {
                tileGroupDic = new Dictionary<string, Tile_Class>();

                xAxis = _x;
                yAxis = _y;
            }
            public void SetTile_Func(Tile_Class _tileClass)
            {
                string _tileType = _tileClass.GetTileGroupType;

                if (this.tileGroupDic.ContainsKey(_tileType) == false)
                {
                    this.tileGroupDic.Add_Func(_tileType, _tileClass);
                }
                else
                {
                    Debug.Log("Set : " + _tileClass.GetTilePosX + "_" + _tileClass.GetTilePosY);
                    Debug.Log("Type : " + _tileType);
                    Debug_C.Warning_Func("다음 Type의 Tile을 추가하려 했으나 이미 동일한 Type의 Tile이 배치되어 있습니다. : " + _tileType);
                }
            }
            public void RemoveTile_Func(Tile_Class _tileClass, bool _isCallTile)
            {
                string _tileType = _tileClass.GetTileGroupType;

                if (this.tileGroupDic.ContainsKey(_tileType) == true)
                {
                    this.tileGroupDic.Remove_Func(_tileType);

                    if (_isCallTile == false)
                        _tileClass.Deactivate_Func();
                }
                else
                {
                    Debug.Log("Remove : " + _tileClass.GetTilePosX + "_" + _tileClass.GetTilePosY);
                    Debug.Log("Type : " + _tileType);
                    Debug_C.Warning_Func("다음 타입의 타일을 제거하려 했으나 배치되어 있지 않습니다. : " + _tileType);
                }
            }
            public bool TryGetTile_Func<TileClass>(string _tileType, out TileClass _tileClass) where TileClass : Tile_Class
            {
                Tile_Class _baseTileClass = null;
                bool _isResult = this.tileGroupDic.TryGetValue(_tileType, out _baseTileClass);
                _tileClass = _baseTileClass as TileClass;

                return _isResult;
            }
            public bool IsTileHave()
            {
                return 0 < this.tileGroupDic.Count;
            }
            public void OnClear_Func()
            {
                foreach (var _item in this.tileGroupDic)
                {
                    Tile_Class _tileClass = this.tileGroupDic[_item.Key];
                    _tileClass.Deactivate_Func();
                }

                this.tileGroupDic.Clear();
            }
        }
        #endregion
        #region Tile_Class
        public abstract class Tile_Class : MonoBehaviour
        {
            protected TileSystem tileSystemClass;
            public abstract string GetTileGroupType { get; }

            public abstract int GetTilePosX { get; }
            public abstract int GetTilePosY { get; }
            public TilePosData GetTilePosData { get { return new TilePosData(GetTilePosX, GetTilePosY); } }

            public virtual void Activate_Func(TilePosData _tilePosData, TileSystem _tileSystemClass, bool _isSetTile = true, bool _isSetPos = true)
            {
                this.Activate_Func(_tilePosData.X, _tilePosData.Y, _tileSystemClass, _isSetTile, _isSetPos);
            }
            public virtual void Activate_Func(Vector2 _tilePos, TileSystem _tileSystemClass, bool _isSetTile = true, bool _isSetPos = true)
            {
                int _posX = (int)_tilePos.x;
                int _posY = (int)_tilePos.y;
                this.Activate_Func(_posX, _posY, _tileSystemClass, _isSetTile, _isSetPos);
            }
            public virtual void Activate_Func(int _x, int _y, TileSystem _tileSystemClass, bool _isSetTile = true, bool _isSetPos = true)
            {
                this.SetPos_Func(_x, _y);

                this.tileSystemClass = _tileSystemClass;

                if (_isSetTile == true) _tileSystemClass.SetTile_Func(this);
                if (_isSetPos == true) this.Move_Func(_x, _y);
            }

            protected void SetPos_Func(TilePosData _posData)
            {
                this.SetPos_Func(_posData.X, _posData.Y);
            }
            protected abstract void SetPos_Func(int _posX, int _posY);

            // 이동 가능 여부
            public virtual bool CheckMovable_Func(Tile_Class _moveTileClass)
            {
                return false;
            }

            // 이동을 시도할 경우 호출됨
            protected bool Move_Func(DirectionType _dirType, bool _isJustCheck = false, params string[] _checkTileTypeArr)
            {
                TilePosData _arrivePosData = TilePosData.GetPos_Func(this, _dirType);

                // 이동 시 확인할 타일 종류가 정해져있는가?
                if (0 < _checkTileTypeArr.Length)
                {

                }
                else
                {
                    // 위 타일과 동일한 타일 종류를 확인한다.
                    _checkTileTypeArr = new string[1] { this.GetTileGroupType };
                }

                bool _isMovable = tileSystemClass.Move_Func(this, _arrivePosData.X, _arrivePosData.Y, _dirType, _isJustCheck, _checkTileTypeArr);

                // 확인만 하는게 아닌가?
                if (_isJustCheck == false)
                {
                    // 이동 가능한가?
                    if (_isMovable == true)
                    {
                        this.SetPos_Func(_arrivePosData);

                        this.Move_Func(_arrivePosData);
                    }
                    else
                    {
                        this.MoveFail_Func();
                    }
                }
                else
                {

                }

                return _isMovable;
            }

            // 이동한 경우 호출됨
            protected virtual void Move_Func(TilePosData _posData)
            {
                this.Move_Func(_posData.X, _posData.Y);
            }
            protected virtual void Move_Func(int _posX, int _posY)
            {
                float _posX_f = _posX * this.tileSystemClass.TileSpace.x;
                float _posY_f = _posY * this.tileSystemClass.TileSpace.y;

                Vector2 _initPos = this.tileSystemClass.TilePos_InitData;
                _posX_f += _initPos.x;
                _posY_f += _initPos.y;

                this.transform.position = new Vector2(_posX_f, _posY_f);
            }

            // 이동에 실패한 경우 호출됨
            protected abstract void MoveFail_Func();

            // 밀려난 경우 호출됨
            public abstract void Pushed_Func(Tile_Class _pushTileClass, DirectionType _pushDir = DirectionType.None);

            public bool CheckTileRange_Func(DirectionType _dirType, out TilePosData _checkPosData, int _times = 1)
            {
                int _checkTilePosX = this.GetTilePosX;
                int _checkTilePosY = this.GetTilePosY;

                switch (_dirType)
                {
                    case DirectionType.Left:
                        _checkTilePosX -= _times;
                        break;
                    case DirectionType.Down:
                        _checkTilePosY -= _times;
                        break;
                    case DirectionType.Up:
                        _checkTilePosY += _times;
                        break;
                    case DirectionType.Right:
                        _checkTilePosX += _times;
                        break;
                }

                _checkPosData = new TilePosData(_checkTilePosX, _checkTilePosY);

                return this.tileSystemClass.CheckInOfRange_Func(_checkTilePosX, _checkTilePosY);
            }

            public virtual void Deactivate_Func(bool _isInit = false)
            {
                if (_isInit == false)
                    this.tileSystemClass.RemoveTile_Func(this, true);
            }
        }
        #endregion

        public enum DirectionType
        {
            Left = -2,
            Down = -1,
            None = 0,
            Up = 1,
            Right = 2,
        }
        public enum WayType
        {
            None = 0,

            Side = 10,
            Exid = 20, // 위 아래를 의미하는 단어를 지웅 형한테 물어보니 이거 알려줌 ^^
        }

        #region TilePosData
        [System.Serializable]
        public struct TilePosData
        {
            [SerializeField] private int x;
            [SerializeField] private int y;

            public TilePosData(int _x, int _y)
            {
                this.x = _x;
                this.y = _y;
            }

            public int X { get { return this.x; } }
            public int Y { get { return this.y; } }
            public bool IsEmpty => this.x == -1 || this.y == -1;

            public static TilePosData GetPos_Func(Tile_Class _tileClass, DirectionType _dirType)
            {
                int _x = _tileClass.GetTilePosX;
                int _y = _tileClass.GetTilePosY;

                TilePosData _data = TilePosData.GetPos_Func(_x, _y, _dirType);

                return _data;
            }
            public static TilePosData GetPos_Func(TilePosData _posData, DirectionType _dirType)
            {
                return TilePosData.GetPos_Func(_posData.x, _posData.y, _dirType);
            }
            public static TilePosData GetPos_Func(int _x, int _y, DirectionType _dirType)
            {
                switch (_dirType)
                {
                    case DirectionType.Up:
                        _y++;
                        break;
                    case DirectionType.Right:
                        _x++;
                        break;
                    case DirectionType.Down:
                        _y--;
                        break;
                    case DirectionType.Left:
                        _x--;
                        break;
                }

                return new TilePosData(_x, _y);
            }
            public static TilePosData GetEmpty_Func() => new TilePosData(-1, -1);

            public static implicit operator TilePosData(Tile_Class _tileClass)
            {
                return _tileClass.GetTilePosData;
            }
            public override string ToString()
            {
                return "(" + this.x + ", " + this.y + ")";
            }
        }
        #endregion

        public partial class TileGroupKey
        {

        }

        public static class TileSystemExtension
        {
            public static DirectionType GetReverseDirType_Func(this DirectionType _dirType)
            {
                switch (_dirType)
                {
                    case DirectionType.Left: return DirectionType.Right;
                    case DirectionType.Down: return DirectionType.Up;
                    case DirectionType.Up: return DirectionType.Down;
                    case DirectionType.Right: return DirectionType.Left;

                    default:
                        Debug_C.Error_Func("_dirType : " + _dirType);
                        return default;
                }
            }
        }
    }

    // 타일 영역이 실시간으로 커지거나 작아지는 기능 추가
    // 
    #endregion
    #region LayerSorting System
    namespace LayerSort
    {
        using Cargold.TileSystem;

        public abstract class LayerSorting_System<T> : MonoBehaviour
        {
            private Dictionary<T, int> layerGapDic;

            // 타일간 레이어 간격
            private int layerGap;
            protected int LayerGap { get { return layerGap; } }

            public virtual void Init_Func()
            {
                layerGapDic = new Dictionary<T, int>();

                T[] _typeArr = Init_LayerType_Func();

                Init_TileGap_Func(_typeArr);
            }
            protected abstract T[] Init_LayerType_Func();

            // 타일간 레이어 간격값
            private void Init_TileGap_Func(params T[] _typeArr)
            {
                int _typeGap = Init_TypeGap_Func();

                for (int i = 0; i < _typeArr.Length; i++)
                {
                    int _keyCount = 0;

                    _keyCount = this.layerGapDic.Keys.Count;

                    int _layerRangeValue = _typeGap * _keyCount;

                    this.layerGapDic.Add_Func(_typeArr[i], _layerRangeValue);

                    // 새로운 타입이 추가되었으므로 타일간 레이어 간격도 그만큼 확장한다.
                    layerGap += _typeGap;
                }
            }
            // 타입간 레이어 간격값
            // 타입 사이에 많은 레이어 구분이 필요할 경우 재정의하여 값을 10보다 키우면 됨
            protected virtual int Init_TypeGap_Func()
            {
                return 10;
            }

            public void SetLayerSort_Func(SpriteRenderer _spriteRend, T _layerType, TilePosData _posData, int _layerExtraID = 0)
            {
                this.SetLayerSort_Func(_spriteRend, _layerType, _posData.Y, _layerExtraID);
            }
            public void SetLayerSort_Func(SpriteRenderer _spriteRend, T _layerType, Tile_Class _tileClass, int _layerExtraID = 0)
            {
                this.SetLayerSort_Func(_spriteRend, _layerType, _tileClass.GetTilePosY, _layerExtraID);
            }
            public void SetLayerSort_Func(SpriteRenderer _spriteRend, T _layerType, int _posY, int _layerExtraID = 0)
            {
                int _typeGap = 1;
                if (this.layerGapDic.TryGetValue(_layerType, out _typeGap) == true)
                {

                }
                else
                {
                    Debug_C.Warning_Func("다음 타입의 레이어는 초기화되지 않았습니다. : " + _layerType);
                }

                // 스프라이트의 타일 Y값만큼 타일 간격을 곱하여 레이어를 정렬한다.
                int _layerSortID = _posY * this.LayerGap;

                // 스프라이트의 타입만큼 레이어를 조금 더 정렬한다.
                _layerSortID += _typeGap;

                // 임의 레이어값만큼 레이어를 조금 더 정렬한다.
                _layerSortID += _layerExtraID;

                // 정렬값을 역전하여 Y축 값이 작을 수록 레이어가 앞에 나오도록 한다.
                _layerSortID *= -1;

                _spriteRend.sortingOrder = _layerSortID;
            }
        }
    }
    #endregion
    #region ResourceFindPath
    namespace ResourceFindPath
    {
        // ResourceFindPath 상속 받는 클래스에서 경로를 스크립트에 적어놓고 쓰는 거 추천

        public abstract class ResourceFindPath<PathType>
        {
            private Dictionary<PathType, string> pathDic;

            public virtual void Init_Func()
            {
                pathDic = new Dictionary<PathType, string>();
            }

            public void SetPath_Func(PathType _pathType, string _path)
            {
                pathDic.Add_Func(_pathType, _path);
            }

            public T GetResource_Func<T>(PathType _pathType, bool _isDebug = true) where T : UnityEngine.Object
            {
                string _path = this.pathDic.GetValue_Func(_pathType);

                return this.GetResource_Func<T>(_path, _isDebug);
            }

            // 잘 불러와졌는지 체크
            public T GetResource_Func<T>(string _path, bool _isDebug = true) where T : UnityEngine.Object
            {
                T _returnObj = Resources.Load<T>(_path);
                if (_returnObj == null)
                {
                    if (_isDebug == true)
                    {
                        Debug.LogError("Bug : 데이터 로드 실패");
                        Debug.Log("Path : " + _path);
                    }
                }

                return _returnObj;
            }
            public T[] GetResourceAll_Func<T>(PathType _pathType, bool _isDebug = true) where T : UnityEngine.Object
            {
                string _path = this.pathDic.GetValue_Func(_pathType);

                return this.GetResourceAll_Func<T>(_path, _isDebug);
            }

            // 잘 불러와졌는지 체크
            public T[] GetResourceAll_Func<T>(string _path, bool _isDebug = true) where T : UnityEngine.Object
            {
                T[] _returnObjArr = Resources.LoadAll<T>(_path);
                if (_returnObjArr == null)
                {
                    if (_isDebug == true)
                    {
                        Debug.LogError("Bug : 데이터 로드 실패");
                        Debug.Log("Path : " + _path);
                    }
                }

                return _returnObjArr;
            }

            public ComponentType GetComponentByInstantiateObj_Func<ComponentType>(PathType _pathType)
            {
                GameObject _loadObj = this.GetResource_Func<GameObject>(_pathType);
                GameObject _genObj = GameObject.Instantiate(_loadObj);

                ComponentType _componentType = _genObj.GetComponent<ComponentType>();

                return _componentType;
            }
        }
    }

    #endregion
    #region Data Structure
    namespace DataStructure
    {
        [System.Serializable]
        public sealed class CirculateQueue<T>
        {
            private List<T> circulateList;
            private int circulateID;
            public T GetItem { get { return this.circulateList[this.circulateID]; } }
            public int GetItemNum { get { return this.circulateList.Count; } }

            public CirculateQueue()
            {
                circulateList = new List<T>();

                circulateID = 0;
            }

            public void SetID_Func(int _id)
            {
                this.circulateID = _id;
            }

            public int GetIndexToItem_Func(T _t)
            {
                return circulateList.IndexOf(_t);
            }

            public T GetItemToIndex_Func(int _idx)
            {
                return circulateList[_idx];
            }

            public void Enqueue_Func(T _t)
            {
                circulateList.AddNewItem_Func(_t);
            }

            public void Enqueue_Func(params T[] _tArr)
            {
                for (int i = 0; i < _tArr.Length; i++)
                {
                    circulateList.AddNewItem_Func(_tArr[i]);
                }
            }

            public T Dequeue_Func(bool _isReverse = false)
            {
                if (_isReverse == false)
                {
                    circulateID++;

                    if (circulateID < circulateList.Count)
                    {

                    }
                    else
                    {
                        circulateID = 0;
                    }
                }
                else
                {
                    circulateID--;

                    if (0 <= circulateID)
                    {

                    }
                    else
                    {
                        circulateID = circulateList.Count - 1;
                    }
                }

                return circulateList[circulateID];
            }

            public void Clear_Func()
            {
                this.circulateList.Clear();
            }
        }

        // Generic Queue가 있어서 사용할 필요 없을 듯...?
        public sealed class Queue_C<T>
        {
            private List<T> queueList;
            public int QueueItemNum { get { return this.queueList.Count; } }
            public bool HasItem { get { return 0 < this.queueList.Count ? true : false; } }

            public Queue_C()
            {
                this.queueList = new List<T>();
            }

            public void Enqueue_Func(T _t)
            {
                this.queueList.AddNewItem_Func(_t);
            }
            public T Dequeue_Func()
            {
                T _returnValue = queueList[0];

                this.queueList.Remove(_returnValue);

                return _returnValue;
            }
            public bool Dequeue_Func(out T _tryGet)
            {
                bool _isHave = false;

                if (0 < queueList.Count)
                {
                    _isHave = true;

                    _tryGet = this.Dequeue_Func();
                }
                else
                {
                    _isHave = false;

                    _tryGet = default(T);
                }

                return _isHave;
            }
        }
    }
    #endregion
    #region Curve System
    namespace CurveSystem
    {
        using Cargold.DataStructure;
        using UnityEngine;

        public abstract class CurveSystem_Class : MonoBehaviour
        {
            private Transform curvePivotTrf;                                // 커브 시작지점 트랜스폼
            private Transform curvePointTrf;                                // 커브 지점 트랜스폼

            public virtual float CurveTime_min { get { return 1f; } }       // 커브 시작지점에서부터 도착지점까지 걸리는 최소 시간
            public virtual float CurveTime_max { get { return 2f; } }       // 커브 시작지점에서부터 도착지점까지 걸리는 최소 시간
            public virtual float PushPower_min { get { return 5f; } }       // 커브 시작 시 밀려나는 힘의 최소값
            public virtual float PushPower_max { get { return 10f; } }      // 커브 시작 시 밀려나는 힘의 최대값

            private CirculateQueue<CurveData> circulateQueue;               // 선형큐를 활용한 커브 데이터 풀링

            private int RandNum { get { return 10; } }                      // 커브 데이터 풀링 개수

            public virtual void Init_Func()
            {
                circulateQueue = new CirculateQueue<CurveData>();
                for (int i = 0; i < RandNum; i++)
                {
                    CurveData _curveData = this.GetDataByManager_Func();

                    circulateQueue.Enqueue_Func(_curveData);
                }

                curvePivotTrf = new GameObject().transform;
                curvePivotTrf.SetParent(this.transform);
                curvePointTrf = new GameObject().transform;
                curvePointTrf.SetParent(curvePivotTrf);
            }
            public void OnCurve_Func(CurvedClass _curvedClass, Vector2 _arrviePos)
            {
                Transform _curvedTrf = _curvedClass.CurvedTrf;

                curvePivotTrf.position = _curvedTrf.position;

                float _curveAngel_Min = _curvedTrf.localEulerAngles.z - _curvedClass.CurveDirectionAngleRange;
                float _curveAngel_Max = _curvedTrf.localEulerAngles.z + _curvedClass.CurveDirectionAngleRange;

                CurveData _curveData = _curvedClass.CurveData;
                Vector3 _curvePos = this.GetCurvePos_Func(_curveData.PushPower, _curveAngel_Min, _curveAngel_Max);

                StartCoroutine(Curve_Cor(_curvedTrf, _curvePos, _arrviePos, _curveData.CurveTime, _curvedClass.IsKeepCurving, _curvedClass.IsLookAtOnCurved, _curvedClass.ArriveCurveDel));
            }
            private IEnumerator Curve_Cor(Transform _curvedTrf, Vector2 _curvePos, Vector2 _arrivePos, float _curveTime, bool _isKeepCurving, bool _isLookAtOnCurved, Action _arriveDel)
            {
                if (_curvedTrf == null) yield break;

                Vector2 _startPos = _curvedTrf.position;

                if (_isKeepCurving == false)
                {
                    yield return Coroutine_C.GetWaitForSeconds_Cor(delegate (float _progressTime)
                    {
                        float _progressRate = _progressTime / _curveTime;

                        Vector2 _movePos = Math_C.GetBezier_Func(_startPos, _curvePos, _arrivePos, _progressRate);

                        if (_isLookAtOnCurved == true)
                            _curvedTrf.LookAt_Func(_movePos);

                        _curvedTrf.transform.position = _movePos;
                    }, _curveTime);
                }
                else
                {
                    float _startTime = Time.time;

                    while (true)
                    {
                        float _progressRate = (Time.time - _startTime) / _curveTime;

                        Vector2 _movePos = Math_C.GetBezier_Func(_startPos, _curvePos, _arrivePos, _progressRate);

                        if (_isLookAtOnCurved == true)
                            _curvedTrf.LookAt_Func(_movePos);

                        _curvedTrf.transform.position = _movePos;

                        yield return null;
                    }
                }

                if (_arriveDel != null)
                    _arriveDel();
            }
            private Vector3 GetCurvePos_Func(float _pushPower, float _curveAngle_Min = 0f, float _curveAngle_Max = 360f)
            {
                return Cargold_Library.GetCircumferencePos_Func(curvePointTrf.position, _pushPower, Random.Range(_curveAngle_Min, _curveAngle_Max));
            }
            public CurveData GetCurveData_Func()
            {
                return circulateQueue.Dequeue_Func();
            }

            public CurveData GetDataByManager_Func()
            {
                float _curvingTime = Random.Range(this.CurveTime_min, this.CurveTime_max);
                float _pushPower = Random.Range(this.PushPower_min, this.PushPower_max);

                CurveData _data = new CurveData(_curvingTime, _pushPower);

                return _data;
            }
        }
        [System.Serializable]
        public struct CurveData
        {
            [SerializeField] private float curvingTime;     // 시작지점부터 도착지점까지 이동에 걸리는 총 시간
            [SerializeField] private float pushPower;       // 커브 시작 시 밀려나는 힘

            public float CurveTime { get { return curvingTime; } }
            public float PushPower { get { return pushPower; } }

            public CurveData(float _curvingTime, float _pushPower)
            {
                this.curvingTime = _curvingTime;
                this.pushPower = _pushPower;
            }
        }
        [System.Serializable]
        public class CurvedClass
        {
            private Transform curvedTrf;           // 커브할 트랜스폼
            private Action arriveCurveDel;         // 커브의 도착지점에 다다른 후 호출할 함수
            [SerializeField] private bool isLookAtOnCurved = false;           // 커브할 방향을 바라볼 것인가?
            [SerializeField] private bool isKeepCurving = false;              // 커브의 도착 이후에도 계속 커브하며 이동할 것인가?
            [SerializeField] private float curveDirectionAngleRange = 0f;  // 커브 시작각의 범위 (현재 로테이션 z축을 기준으로 랜덤하게 각이 변할 편차값)
            private CurveData curveData;

            public float CurveDirectionAngleRange => this.curveDirectionAngleRange;

            public Transform CurvedTrf { get { return curvedTrf; } }
            public Action ArriveCurveDel { get { return arriveCurveDel; } }
            public bool IsLookAtOnCurved { get { return isLookAtOnCurved; } }
            public bool IsKeepCurving { get { return isKeepCurving; } }
            public CurveData CurveData { get { return curveData; } }

            public CurvedClass(Transform _curvedTrf, Action _arriveCurveDel, CurveSystem_Class _curveSystemClass)
            {
                this.curvedTrf = _curvedTrf;
                this.arriveCurveDel = _arriveCurveDel;

                if (curveDirectionAngleRange == 0f)
                    this.curveDirectionAngleRange = 180f;

                this.curveData = _curveSystemClass.GetCurveData_Func();
            }

            public CurvedClass(Transform _curvedTrf, Action _arriveCurveDel, CurveData _curveData)
            {
                this.curvedTrf = _curvedTrf;
                this.arriveCurveDel = _arriveCurveDel;

                if (curveDirectionAngleRange == 0f)
                    this.curveDirectionAngleRange = 180f;

                this.curveData = _curveData;
            }
        }
    }
    #endregion
    #region Joystick
    namespace Joystick
    {
        using UnityEngine.EventSystems;

        public class JoyStickController_Script : MonoBehaviour
        {
            [SerializeField] protected Transform stickTrf = null;
            [SerializeField] private float radius = 0f;           // 조이스틱 배경의 반 지름.
            [SerializeField] private Transform bgTrf = null;

#if ODIN_INSPECTOR
            [Sirenix.OdinInspector.ShowInInspector, Sirenix.OdinInspector.ReadOnly]
#endif
            protected Vector2 stickInitPos;  // 조이스틱의 처음 위치.
            private Coroutine dragCor;

            public virtual void Init_Func()
            {
                if (radius == 0f)
                    radius = this.GetComponent<RectTransform>().sizeDelta.y * 0.5f;

                stickInitPos = stickTrf.transform.position;

                if (bgTrf == null)
                    bgTrf = this.transform;

                this.Deactive_Func(true);
            }

            public void Active_Func(Vector2 _touchPos)
            {
                bgTrf.gameObject.SetActive(true);
                bgTrf.position = _touchPos;

                stickInitPos = _touchPos;
                stickTrf.position = _touchPos;

                this.dragCor = StartCoroutine(StickDragChecker_Cor());
            }
            private IEnumerator StickDragChecker_Cor()
            {
                while (true)
                {
                    this.OnDragging_Func(stickTrf.position);

                    yield return null;
                }
            }
            protected virtual void OnDragging_Func(Vector2 _stickPos)
            {

            }

            public Vector2 GetJoyDir_Func(Vector2 _dragPos)
            {
                return (_dragPos - stickInitPos).normalized;
            }
            public Vector2 GetJoyDirByDistance_Func(Vector2 _dragPos)
            {
                Vector2 _joyDir = this.GetJoyDir_Func(_dragPos);

                // 조이스틱의 초기 위치와 현재 내 터치 위치와의 거리를 구한다.
                float _dist = Vector3.Distance(_dragPos, stickInitPos);

                // 거리가 반지름보다 작으면 방향과 거리를 곱하고 반환
                if (_dist < radius) return _joyDir * _dist;

                // 거리가 반지름보다 크면 방향에 반지름 크기까지만 곱하고 반환
                else return _joyDir * radius;
            }
            public void SetDragStick_Func(Vector2 _dragPos)
            {
                // 조이스틱 방향 계산
                Vector2 _joyDir = Vector2.zero;

                this.SetDragStick_Func(_dragPos, out _joyDir);
            }
            public void SetDragStick_Func(Vector2 _dragPos, out Vector2 _joyDir)
            {
                // 조이스틱 방향 계산
                _joyDir = this.GetJoyDirByDistance_Func(_dragPos);

                stickTrf.position = _joyDir + stickInitPos;
            }

            public float GetJoyAngle_Func(Vector2 _dragPos)
            {
                //해당 조이스틱의 각도를 계산
                float _angle = Mathf.Atan2(_dragPos.y - stickInitPos.y, _dragPos.x - stickInitPos.x) * 180 / Mathf.PI;

                // 0도가 위를 향하게끔 보정
                _angle -= 90f;

                // 음수가 없게끔 보정
                if (_angle < 0) _angle += 360;

                // 시계방향으로 각이 형성되게끔 보정
                _angle = 360f - _angle;
                return _angle;
            }

            // 드래그 끝.
            public virtual void Deactive_Func(bool _isInit = false)
            {
                if (_isInit == false)
                {
                    if (this.dragCor != null)
                        StopCoroutine(this.dragCor);

                    this.stickTrf.position = stickInitPos; // 스틱을 원래의 위치로.
                }

                this.bgTrf.gameObject.SetActive(false);
            }
        }
    }
    #endregion
    #region Reserve
    namespace ReserveSystem
    {
        [System.Serializable]
        public class ReserveSystem
        {
            private float timer;
            private bool isReserve;
            private float nextChangeTime;
            private Action callback;
            private Coroutine cor;
            private MonoBehaviour coroutineCallerObj;

            public ReserveSystem(float _nextChangeTime, Action _callback, MonoBehaviour _coroutineCallerObj = null)
            {
                this.nextChangeTime = _nextChangeTime;
                this.callback = _callback;
                this.coroutineCallerObj = _coroutineCallerObj is null == false ? _coroutineCallerObj : Coroutine_C.GetMonoBehaviour;
            }

            public void Activate_Func()
            {
                this.timer = Time.unscaledTime;
                this.isReserve = false;

                this.cor = coroutineCallerObj.StartCoroutine(this.Reserve_Cor());
            }
            public void OnReserve_Func(bool _isImmediate = false)
            {
                if (_isImmediate == false)
                    this.isReserve = true;
                else
                    this.callback();
            }
            public IEnumerator Reserve_Cor()
            {
                while (true)
                {
                    if (this.isReserve == true)
                    {
                        if (this.timer <= Time.unscaledTime)
                        {
                            this.OnImmediately_Func();
                        }
                    }

                    yield return null;
                }
            }

            private void OnImmediately_Func()
            {
                this.timer = Time.unscaledTime + this.nextChangeTime;
                this.isReserve = false;

                this.callback();
            }

            public void Deactivate_Func()
            {
                this.coroutineCallerObj.StopCoroutine(this.cor);
                this.cor = null;
            }
        }
    }
    #endregion
    #region EnumCompare
    namespace EnumCompare
    {
        using Unity.Collections.LowLevel.Unsafe;

        public class EnumCompare<T> : IEqualityComparer<T> where T : struct, IConvertible
        {
            private static EnumCompare<T> instance;
            public static EnumCompare<T> Instance
            {
                get
                {
                    if (EnumCompare<T>.instance == null)
                        EnumCompare<T>.instance = new EnumCompare<T>();

                    return EnumCompare<T>.instance;
                }
            }

            bool IEqualityComparer<T>.Equals(T _x, T _y)
            {
                int _xInt = 0;
                int _yInt = 0;

#if UNITY_2018_1_OR_NEWER
                _xInt = UnsafeUtility.EnumToInt(_x);
                _yInt = UnsafeUtility.EnumToInt(_y);
#else
            _xInt = EnumCompare<T>.Enum32ToInt(_x);
            _yInt = EnumCompare<T>.Enum32ToInt(_y);
#endif

                return _xInt == _yInt;
            }

            int IEqualityComparer<T>.GetHashCode(T _obj)
            {
#if UNITY_2018_1_OR_NEWER
                return UnsafeUtility.EnumToInt(_obj);
#else
            return EnumCompare<T>.Enum32ToInt(_obj);
#endif
            }

#if UNITY_2018_1_OR_NEWER

#else
        public static int Enum32ToInt(T _enumValue)
        {
            Shell _shell = new Shell();
            _shell.EnumValue = _enumValue;

            unsafe
            {
                int* pi = &_shell.IntValue;
                pi += 1;
                return *pi;
            }
        }

        public static T IntToEnum32(int _value)
        {
            Shell _shell = new Shell();

            unsafe
            {
                int* pi = &_shell.IntValue;
                pi += 1;
                *pi = _value;
            }

            return _shell.EnumValue;
        }

        private struct Shell
        {
            public int IntValue;
            public T EnumValue;
        }
#endif

            public static Dictionary<T, Value> GetEnumDic_Func<Value>(Dictionary<T, Value> _baseDic)
            {
                Dictionary<T, Value> _enumDic = new Dictionary<T, Value>(EnumCompare<T>.Instance);

                foreach (KeyValuePair<T, Value> item in _baseDic)
                    _enumDic.Add(item.Key, item.Value);

                return _enumDic;
            }
        }
    }
    #endregion
    #region Gacha
    namespace Gacha
    {
        [System.Serializable]
        public class GachaSystem<T>
        {
            public Node rootNode;

            private Data[] dataArr;
            private int totalFP;
            private int splitCount;
            private GachaResultData<T>[] gachaResultDataArr;

            public int TotalFP { get => totalFP; }

            public GachaSystem(GachaResultData<T>[] _gachaResultDataArr, int _splitCount = 2)
            {
                this.gachaResultDataArr = _gachaResultDataArr;

                int _dataNum = _gachaResultDataArr.Length;
                this.dataArr = new Data[_dataNum];
                this.totalFP = 0;
                this.splitCount = _splitCount;

                for (int i = 0; i < _dataNum; i++)
                {
                    dataArr[i] = new Data();
                    dataArr[i].arrID = i;
                    int _FPvalue = _gachaResultDataArr[i].floatingProbability;
                    dataArr[i].floatingProbability = _FPvalue;

                    totalFP += _FPvalue;
                }

                rootNode = GetNode_Func(null, -1, 0, _dataNum, 0, this.totalFP);
            }
            private Node GetNode_Func(Node _parentNode, int _siblingLv, int _start, int _end, int _loadedFP, int _totalCalcFP)
            {
                _siblingLv++;

                Node _node = null;

                if (_start < _end &&
                    (_end != this.dataArr.Length || _start + 1 != this.dataArr.Length))
                {
                    _node = new Node();
                    _node.childNodeArr = new Node[this.splitCount];

                    int _addFP = 0;
                    int _splitFP = _totalCalcFP / this.splitCount;
                    int _addSplitFP = _splitFP;
                    int _childNodeID = 0;
                    for (int i = _start; i < _end; i++)
                    {
                        int _FPvalue = dataArr[i].floatingProbability;
                        _addFP += _FPvalue;

                        if (_addSplitFP < _addFP)
                        {
                            Node _childNode = GetNode_Func(_node, _siblingLv, _start, i, _loadedFP, _totalCalcFP - _addFP);
                            _node.childNodeArr[_childNodeID] = _childNode;

                            _childNodeID++;

                            if (_childNodeID + 1 < this.splitCount)
                            {
                                _addSplitFP = (_childNodeID + 1) * _splitFP;
                            }
                            else
                            {
                                _childNode = GetNode_Func(_node, _siblingLv, i + 1, _end, _loadedFP + _addFP, _totalCalcFP - _addFP);
                                _node.childNodeArr[_childNodeID] = _childNode;

                                break;
                            }
                        }
                    }

                    if (0 < _siblingLv)
                    {
                        _node.nodeType = NodeType.Normal;
                    }
                    else
                    {
                        _node.nodeType = NodeType.Root;
                    }
                }
                else
                {
                    LeafNode _leafNode = new LeafNode();

                    _leafNode.data = this.dataArr[_start];
                    _leafNode.nodeType = NodeType.Leaf;
                    _node = _leafNode;
                }

                _node.siblingLv = _siblingLv;
                _node.FP = _loadedFP;
                _node.parentNode = _parentNode;

                return _node;
            }

            public T GetGachaResultType_Func()
            {
                int _checkFP = UnityEngine.Random.Range(0, this.totalFP) + 1;
                int _resultID = GetGachaID_Func(_checkFP, this.rootNode.childNodeArr);
                return this.gachaResultDataArr[_resultID].resultType;
            }
            private static int GetGachaID_Func(int _checkFP, Node[] _nodeArr)
            {
                for (int i = _nodeArr.Length - 1; 0 <= i; i--)
                {
                    NodeType _nodeType = _nodeArr[i].nodeType;
                    if (_nodeType == NodeType.Normal)
                    {
                        if (_nodeArr[i].FP < _checkFP)
                        {
                            return GetGachaID_Func(_checkFP, _nodeArr[i].childNodeArr);
                        }
                    }
                    else if (_nodeType == NodeType.Leaf)
                    {
                        LeafNode _leafNode = _nodeArr[i] as LeafNode;

                        if (_leafNode.FP < _checkFP)
                        {
                            return _leafNode.data.arrID;
                        }
                    }
                    else
                    {
                        Debug.LogError("_nodeType : " + _nodeType);
                        break;
                    }
                }

                Debug.LogError("_checkFP : " + _checkFP);

                return 0;
            }
        }

        [System.Serializable]
        public class Node
        {
            public int siblingLv;
            public int FP;
            public NodeType nodeType;

            public Node parentNode;
            public Node[] childNodeArr;
        }

        [System.Serializable]
        public class LeafNode : Node
        {
            public Data data;
        }

        [System.Serializable]
        public struct Data
        {
            public int arrID;
            public int floatingProbability;
        }

        [System.Serializable]
        public struct GachaResultData<T>
        {
            public T resultType;
            public int floatingProbability;
        }

        public enum NodeType
        {
            None = 0,

            Root,
            Normal,
            Leaf,
        }
    }
    #endregion
    #region TagDictionary
    namespace TagDictionary
    {
        [System.Serializable]
        public class TagDictionary<Key, Value> where Value : ITagDic<Key>
        {
            private Dictionary<Key, List<Value>> dic;

            public TagDictionary(params Key[] _keyArr)
            {
                this.dic = new Dictionary<Key, List<Value>>();

                this.GenerateKey_Func(_keyArr);
            }
            public TagDictionary(IEqualityComparer<Key> _iEqualityComparer, params Key[] _keyArr)
            {
                this.dic = new Dictionary<Key, List<Value>>(_iEqualityComparer);

                this.GenerateKey_Func(_keyArr);
            }

            public void GenerateKey_Func(params Key[] _keyArr)
            {
                dic = new Dictionary<Key, List<Value>>();

                for (int i = 0; i < _keyArr.Length; i++)
                {
                    List<Value> _list = new List<Value>();

                    this.dic.Add(_keyArr[i], _list);
                }
            }

            public void Add_Func(Value _value)
            {
                Key[] _keyArr = _value.GetTagKey_Func();

                foreach (Key _key in _keyArr)
                {
                    List<Value> _list = this.dic.GetValue_Func(_key);
                    _list.AddNewItem_Func(_value);
                }
            }

            public bool TryGetListValue_Func(Key _key, out List<Value> _valueList)
            {
                return this.dic.TryGetValue(_key, out _valueList);
            }

            public void Remove_Func(Value _value)
            {
                Key[] _keyArr = _value.GetTagKey_Func();

                foreach (Key _key in _keyArr)
                {
                    List<Value> _list = this.dic.GetValue_Func(_key);
                    _list.Remove_Func(_value);
                }
            }

            public bool IsEmpty_Func()
            {
                foreach (var item in this.dic)
                {
                    if (item.Value.Count != 0)
                        return false;
                }

                return true;
            }
        }

        public interface ITagDic<KeyType>
        {
            KeyType[] GetTagKey_Func();
        }
    }
    #endregion
    #region TimeSystem_Manager
    namespace TimeSystem
    {
        using Cargold.FrameWork;

        public class TimeSystem_Manager
        {
            protected static TimeSystem_Manager instance;
            public static TimeSystem_Manager Instance
            {
                get
                {
                    if (instance == null)
                        instance = new TimeSystem_Manager();

                    return instance;
                }
            }

            private Cargold.Observer.Observer_Action midnightObs = new Cargold.Observer.Observer_Action();
            private Cargold.Observer.Observer_Action<DateTime> timerObs = new Cargold.Observer.Observer_Action<DateTime>();
            protected DateTimeTick playTimeTick;

            public virtual DateTime Now
            {
                get
                {
                    DateTime _nowTime = DateTime.Now;
#if UNITY_EDITOR
                    _nowTime = this.GetOffsetTime_Func(_nowTime);
#endif
                    return _nowTime;
                }
            }
            public DateTime GetPlayTime
            {
                get
                {
                    long _ticks = this.Now.Ticks - this.playTimeTick.tick;
                    if (_ticks < 0L)
                        _ticks = 0L;

                    return new DateTime(_ticks);
                }
            }

            public TimeSystem_Manager()
            {
                instance = this;

                Coroutine_C.StartCoroutine_Func(CheckMidnight_Cor());

                Coroutine_C.StartCoroutine_Func(OnTimer_Cor());

                this.playTimeTick = this.Now;
            }

            public void ResetPlayTime_Func()
            {
                this.playTimeTick = this.Now;
                Debug.Log("ResetPlayTime_Func) this.playTimeTick : " + this.playTimeTick.GetTime);
            }
            private IEnumerator CheckMidnight_Cor()
            {
                DateTime _midnightTime = this.GetMidNight_Func();

                while (true)
                {
                    if (_midnightTime < this.Now)
                    {
                        _midnightTime = this.GetMidNight_Func();

                        midnightObs.Notify_Func();
                    }

                    yield return Coroutine_C.GetWaitForSeconds_Cor(.5f, true);
                }
            }
            private IEnumerator OnTimer_Cor()
            {
                while (true)
                {
                    timerObs.Notify_Func(this.Now);

                    yield return Coroutine_C.GetWaitForSeconds_Cor(.5f, true);
                }
            }

            public DateTime GetMidNight_Func()
            {
                return this.Now.GetMidNight_Func();
            }
            public TimeSpan GetRemainMidNight_Func()
            {
                DateTime _now = this.Now;
                DateTime _midnight = _now.GetMidNight_Func();
                return _midnight - _now;
            }
            public DateTime GetNextDayOfWeek_Func()
            {
                return this.Now.GetNextDayOfWeek_Func(DayOfWeek.Monday);
            }
            public DateTime GetNextBeginningOfMonth_Func()
            {
                return this.Now.GetNextBeginningOfMonth_Func();
            }
            public DateTime GetAfterDay_Func(int _days, bool _isMidnight = false)
            {
                return this.Now.GetAfterDay_Func(_days, _isMidnight);
            }
            public DateTime GetOffsetTime_Func(DateTime _dateTime)
            {
//#if UNITY_EDITOR
//                _dateTime = _dateTime.AddDays(DataBase_Manager.Instance.GetDefine.testAddDay);
//                _dateTime = _dateTime.AddHours(DataBase_Manager.Instance.GetDefine.testAddHour);
//                _dateTime = _dateTime.AddMinutes(DataBase_Manager.Instance.GetDefine.testAddMin);
//                _dateTime = _dateTime.AddSeconds(DataBase_Manager.Instance.GetDefine.testAddSec);
//#endif

                return _dateTime;
            }

            public void Subscribe_Midnight_Func(Action _del)
            {
                midnightObs.Subscribe_Func(_del);
            }
            public bool TrySubscribe_Midnight_Func(Action _del)
            {
                if (midnightObs.IsSubscribed_Func(_del) == false)
                {
                    midnightObs.Subscribe_Func(_del);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            public void Unsubscribe_Midnight_Func(Action _del)
            {
                midnightObs.Unsubscribe_Func(_del);
            }

            public bool IsSubscribe_OnTimer_Func(Action<DateTime> _del)
            {
                return timerObs.IsSubscribed_Func(_del);
            }
            public void Subscribe_OnTimer_Func(Action<DateTime> _del, bool _isCallback = true)
            {
                timerObs.Subscribe_Func(_del);

                if (_isCallback == true)
                    _del(this.Now);
            }
            public void Unsubscribe_OnTimer_Func(Action<DateTime> _del)
            {
                timerObs.Unsubscribe_Func(_del);
            }
        }
    }
    #endregion
    #region DateTimeTick
    [System.Serializable]
    public struct DateTimeTick
    {
        [HideInInspector] public long tick;
        public DateTime GetTime => new DateTime(this.tick);
        public DateTime GetRemainTime
        {
            get
            {
                DateTime _nowTime = TimeSystem_Manager.Instance.Now;
                DateTime _thisTime = this.GetTime;
                if (_nowTime <= _thisTime)
                    return new DateTime((_thisTime - _nowTime).Ticks);
                else
                    return default;
            }
        }
        public TimeSpan GetPassTime
        {
            get
            {
                DateTime _thisTime = this.GetTime;
                DateTime _nowTime = TimeSystem_Manager.Instance.Now;
                if (_thisTime <= _nowTime)
                    return _nowTime - _thisTime;
                else
                    return default;
            }
        }
#if UNITY_EDITOR
#if ODIN_INSPECTOR
        [Sirenix.OdinInspector.ShowInInspector] public string GetTimeStr => this.GetTime.ToString();
#endif
#endif

        public DateTimeTick(DateTime _dateTime)
        {
            this.tick = _dateTime.Ticks;
        }
        public DateTimeTick(TimeSpan _timeSpan)
        {
            this.tick = _timeSpan.Ticks;
        }
        public DateTimeTick(long _tick)
        {
            this.tick = _tick;
        }

        public static implicit operator DateTimeTick(DateTime _dateTime) => new DateTimeTick(_dateTime);
        public static implicit operator DateTimeTick(TimeSpan _timeSpan) => new DateTimeTick(_timeSpan);
        public static implicit operator DateTimeTick(long _tick) => new DateTimeTick(_tick);
        public static implicit operator DateTime(DateTimeTick _tick) => _tick.GetTime;
        // GetRemainTime을 호출할 시 현재 시간보다 늦춰지면 마이너스라 에러를 뱉을 듯. 이럴 경우 0을 뱉도록 예외처리 ㄱㄱ

        public void SetNow_Func()
        {
            this.tick = TimeSystem_Manager.Instance.Now.Ticks;
        }
    }
    #endregion

    #region Tween_C
    public static class Tween_C
    {
        //private static Dictionary<Transform, TweenData> baseTwnDic;
        //private static Dictionary<Transform, TweenData> twnDic
        //{
        //    get
        //    {
        //        if (baseTwnDic == null)
        //            baseTwnDic = new Dictionary<Transform, TweenData>();

        //        return baseTwnDic;
        //    }
        //}

        //public static void OnPunch_Func(Transform _punchTrf, float _punchPower = -1f, float _duration = -1f, float _originScale = 1f)
        //{
        //    if (twnDic.TryGetValue(_punchTrf, out TweenData _twnData) == false)
        //    {
        //        _twnData = new TweenData();

        //        _twnData.originScale = Vector3.one * _originScale;

        //        if (_punchPower == -1f)
        //            _punchPower = Cargold.FrameWork.DataBase_Manager.Instance.GetUi.twn_Power;

        //        if (_duration == -1f)
        //            _duration = Cargold.FrameWork.DataBase_Manager.Instance.GetUi.twn_Duraion;

        //        Tween _twn = _punchTrf.DOPunchScale(Vector3.one * _punchPower, _duration).OnComplete(delegate ()
        //        {
        //            _punchTrf.localScale = Vector3.one * _originScale;
        //        }).SetAutoKill(false);

        //        _twn.Pause();

        //        _twnData.twn = _twn;

        //        twnDic.Add_Func(_punchTrf, _twnData);
        //    }

        //    _punchTrf.localScale = _twnData.originScale;

        //    _twnData.twn.Restart();
        //}

        //public class TweenData
        //{
        //    public Tween twn;
        //    public Vector3 originScale;
        //}
    }
    #endregion
    #region UGUI_C
    public class UGUI_C
    {
        public static void SetContentGroupResize_Func
            (RectTransform _contentGroupRtrf, float _groupSpace = 0f, float _topSpace = 0f, float _bottomSpace = 0f, params ContentGroupData[] _dataArr)
        {
            float _stackHeight = _topSpace;

            for (int i = 0; i < _dataArr.Length; i++)
            {
                ContentGroupData _data = _dataArr[i];
                RectTransform _contentRtrf = _data.contentRtrf;
                float _height = _data.height;
                _contentRtrf.SetHeight_Func(_height);

                //if (0 < i)
                _contentRtrf.anchoredPosition = Vector3.down * _stackHeight;

                _stackHeight += _height + _groupSpace;
            }

            _contentGroupRtrf.SetHeight_Func(_stackHeight + _bottomSpace);
        }

        [System.Serializable]
        public struct ContentGroupData
        {
            public float height;
            public RectTransform contentRtrf;

            public ContentGroupData(float height, RectTransform contentRtrf)
            {
                this.height = height;
                this.contentRtrf = contentRtrf;
            }
        }
    }
    #endregion
    #region StringFormat_C
    public static class StringFormat_C
    {
        public static string GetFormatting_Func(string _base, IFormatter _iFormat)
        {
            int _formattingNum = _iFormat.FormattingNum;

            switch (_formattingNum)
            {
                case 0:
                    return _base;

                case 1:
                    return string.Format(_base
                    , _iFormat.GetFormattingStr_Func(0)
                    );

                case 2:
                    return string.Format(_base
                    , _iFormat.GetFormattingStr_Func(0)
                    , _iFormat.GetFormattingStr_Func(1)
                    );

                case 3:
                    return string.Format(_base
                    , _iFormat.GetFormattingStr_Func(0)
                    , _iFormat.GetFormattingStr_Func(1)
                    , _iFormat.GetFormattingStr_Func(2)
                    );

                case 4:
                    return string.Format(_base
                    , _iFormat.GetFormattingStr_Func(0)
                    , _iFormat.GetFormattingStr_Func(1)
                    , _iFormat.GetFormattingStr_Func(2)
                    , _iFormat.GetFormattingStr_Func(3)
                    );

                case 5:
                    return string.Format(_base
                    , _iFormat.GetFormattingStr_Func(0)
                    , _iFormat.GetFormattingStr_Func(1)
                    , _iFormat.GetFormattingStr_Func(2)
                    , _iFormat.GetFormattingStr_Func(3)
                    , _iFormat.GetFormattingStr_Func(4)
                    );

                default:
                    Debug.LogError("_formattingNum : " + _formattingNum);
                    return _base;
            }
        }

        public interface IFormatter
        {
            int FormattingNum { get; }
            string GetFormattingStr_Func(int _id);
        }
    }
    #endregion
    #region StringBuilder_C
    public static class StringBuilder_C
    {
        public const string Percent = "%";

        private static StringBuilder staticBuilder;
        public static StringBuilder AccessCarefully
        {
            get
            {
                if (staticBuilder == null)
                    staticBuilder = new StringBuilder(1024);

                return staticBuilder;
            }
        }

        public static string Append_Func(params string[] _valueArr)
        {
            if (staticBuilder == null)
                staticBuilder = new StringBuilder(1024);

            staticBuilder.RemoveAll_Func();

            for (int i = 0; i < _valueArr.Length; i++)
            {
                staticBuilder.Append(_valueArr[i]);
            }

            return staticBuilder.ToString();
        }

        public static string GetTensionTime_Func(float _remianTime)
        {
            return 1f <= _remianTime
                        ? ((int)_remianTime).ToString()
                        : _remianTime.ToString_Func(1);
        }
    }
    #endregion
    #region Coroutine_C
    public static class Coroutine_C
    {
        public static MonoBehaviour GetMonoBehaviour => CoroutineClass_C.Instance;

        private class CoroutineClass_C : Cargold.Singleton.Singleton_Func<CoroutineClass_C>
        {
            private bool isInitialize = false;
            private Dictionary<string, Coroutine> coroutineDic;

            public bool IsInitialize { get { return isInitialize; } }

            public override void Init_Func()
            {
                if (isInitialize == false)
                {
                    isInitialize = true;

                    coroutineDic = new Dictionary<string, Coroutine>();
                }
            }

            public Coroutine StartCoroutine_Func(IEnumerator _enumerator, string _key = "")
            {
                Coroutine _cor = this.StartCoroutine_Func(_enumerator);
                this.coroutineDic.Add_Func(_key, _cor);

                return _cor;
            }
            public Coroutine StartCoroutine_Func(IEnumerator _enumerator)
            {
                return this.StartCoroutine(_enumerator);
            }
            public void StopCoroutine_Func(string _key)
            {
                Coroutine _cor = null;
                if (this.coroutineDic.TryRemove_Func(_key, out _cor) == true)
                    StopCoroutine(_cor);
            }
        }

        private static WaitForFixedUpdate waitForFixedUpdate;
        public static WaitForFixedUpdate WaitForFixedUpdate
        {
            get
            {
                if (waitForFixedUpdate == null) waitForFixedUpdate = new WaitForFixedUpdate();

                return waitForFixedUpdate;
            }
        }

        public static IEnumerator GetWaitForSeconds_Cor(float _time = 0.02f, bool _isUnscaledTime = false)
        {
            if (_isUnscaledTime == false)
            {
                float _loopBeginTime = Time.time;

                while (Time.time < _loopBeginTime + _time)
                {
                    yield return null;
                }
            }
            else
            {
                float _loopBeginTime = Time.unscaledTime;

                while (Time.unscaledTime < _loopBeginTime + _time)
                {
                    yield return null;
                }
            }
        }
        public static IEnumerator GetWaitForSeconds_Cor(Action<float> _progressTimeDel, float _time = 0.02f)
        {
            float _loopBeginTime = Time.time;

            while (Time.time < _loopBeginTime + _time)
            {
                if (_progressTimeDel != null)
                    _progressTimeDel(Time.time - _loopBeginTime);

                yield return null;
            }
        }

        public static Coroutine StartCoroutine_Func(IEnumerator _enumerator, string _key = "")
        {
            if (CoroutineClass_C.Instance.IsInitialize == false)
                CoroutineClass_C.Instance.Init_Func();

            return CoroutineClass_C.Instance.StartCoroutine_Func(_enumerator, _key);
        }
        public static Coroutine StartCoroutine_Func(IEnumerator _enumerator)
        {
            return CoroutineClass_C.Instance.StartCoroutine_Func(_enumerator);
        }
        public static void StopCoroutine_Func(Coroutine _cor)
        {
            CoroutineClass_C.Instance.StopCoroutine(_cor);
        }
        public static void StopCoroutine_Func(string _key)
        {
            if (CoroutineClass_C.Instance.IsInitialize == false)
                CoroutineClass_C.Instance.Init_Func();

            CoroutineClass_C.Instance.StopCoroutine_Func(_key);
        }
        public static void StopAllCoroutine_Func()
        {
            CoroutineClass_C.Instance.StopAllCoroutines();
        }

        public static Coroutine Invoke_Func(Action _del)
        {
            return CoroutineClass_C.Instance.StartCoroutine(Coroutine_C.Invoke_Cor(_del));
        }
        private static IEnumerator Invoke_Cor(Action _del)
        {
            yield return null;

            _del();
        }
        public static Coroutine Invoke_Func(Action _del, float _time, bool _isUnscaledTime = false)
        {
            return CoroutineClass_C.Instance.StartCoroutine(Coroutine_C.Invoke_Cor(_del, _time, _isUnscaledTime));
        }
        private static IEnumerator Invoke_Cor(Action _del, float _time, bool _isUnscaledTime = false)
        {
            yield return Coroutine_C.GetWaitForSeconds_Cor(_time, _isUnscaledTime);

            _del();
        }
    }
    #endregion
    #region Random_C
    public static class Random_C
    {
        public static float GetValue => UnityEngine.Random.value;
        public static int Random_Func(int _min, int _max)
        {
            return UnityEngine.Random.Range(_min, _max);
        }
        public static float Random_Func(float _min, float _max)
        {
            return UnityEngine.Random.Range(_min, _max);
        }
        public static bool CheckPercent_Func(int _maxPercent, int _checkPercent = 1)
        {
            return UnityEngine.Random.Range(0, _maxPercent) <= (_checkPercent - 1);
        }
        public static T GetRandomItem_Func<T>(T _item1, T _item2)
        {
            return UnityEngine.Random.value < .5f == true ? _item1 : _item2;
        }
        public static T GetRandomItem_Func<T>(T _item1, T _item2, T _item3)
        {
            float _value = UnityEngine.Random.value;
            if (_value < 0.333f)
                return _item1;
            else if (_value < 0.666f)
                return _item2;
            else
                return _item3;
        }
        public static T GetRandomItem_Func<T>(T _item1, T _item2, T _item3, T _item4)
        {
            float _value = UnityEngine.Random.value;
            if (_value < 0.25f)
                return _item1;
            else if (_value < 0.5f)
                return _item2;
            else if (_value < 0.75f)
                return _item3;
            else
                return _item4;
        }

        public static bool IsRandom_Func(this float _value)
        {
            return UnityEngine.Random.value <= _value;
        }
    }
    #endregion
    #region Math_C
    public static class Math_C
    {
        public static Quaternion GetLookAt_Func(Vector3 _thisPos, Vector3 _targetPos)
        {
            float angle = _thisPos.GetAngle_Func(_targetPos, true);

            Quaternion rotation = new Quaternion();
            rotation.eulerAngles = new Vector3(0f, 0f, angle);
            return rotation;
        }

        public static Vector3 GetBezier_Func(Vector3 _startPos, Vector3 _curvePos, Vector3 _arrivePos, float _time)
        {
            var omt = 1f - _time;
            return _startPos * omt * omt + 2f * _curvePos * omt * _time + _arrivePos * _time * _time;
        }
        public static Vector2 GetBezier_Func(Vector2 _startPos, Vector2 _curvePos, Vector2 _arrivePos, float _time)
        {
            var omt = 1f - _time;
            return _startPos * omt * omt + 2f * _curvePos * omt * _time + _arrivePos * _time * _time;
        }

        public static ReturnValue Random_Func<ReturnValue>(params ReturnValue[] _randValueArr)
        {
            return _randValueArr.GetRandItem_Func();
        }

        public static float Case_Func(float _value, float _min, float _max)
        {
            int _valueInt = (int)(_value * 1000f);
            int _minInt = (int)(_min * 1000f);
            int _maxInt = (int)(_max * 1000f);

            int _caseValue = Math_C.Case_Func(_valueInt, _minInt, _maxInt);
            return _caseValue * 0.001f;
        }
        public static int Case_Func(int _value, int _min, int _max)
        {
            int _caseGap = _max - _min;
            int _remainderValue = 0;

            if (0 < _caseGap)
            {
                Math.DivRem(_value, _caseGap + 1, out _remainderValue);

                return 0 <= _remainderValue
                    ? _min + _remainderValue
                    : _max + 1 + _remainderValue;
            }
            else if (_caseGap == 0)
            {
                return _max;
            }
            else
            {
                Debug.LogError("Max value is lower than Min value");
                return -1;
            }
        }

        // 원의 중심에서 _angle에 해당하는 원 둘레의 좌표 얻어오기
        public static Vector2 GetCircumferencePos_Func(Vector2 _circleCenterPos, float _radius, float _angle)
        {
            float _calcAngle = (_angle * -1f + 90f) * Mathf.Deg2Rad;
            float _cos = _radius * Mathf.Cos(_calcAngle);
            float _sin = _radius * Mathf.Sin(_calcAngle);

            return _circleCenterPos += new Vector2(_cos, _sin);
        }

        // 두 좌표 사이에 각도 구하기
        public static float GetAngle_Func(Vector2 _thisPos, Vector2 _targetPos, bool _isRelativeToRotate = false)
        {
            Vector2 _normalTangent = _targetPos - _thisPos;
            _normalTangent.Normalize();

            float angle = Mathf.Atan2(_normalTangent.x, _normalTangent.y) * Mathf.Rad2Deg;

            float _returnAngle = 0f <= angle ? angle : 360f + angle;

            if (_isRelativeToRotate == false)
                return _returnAngle;
            else
                return _returnAngle * -1f;
        }

        // 시작점에서 목표점까지 직진할 때 먼저 닿는 사각형의 모서리 위치
        public static Vector2 GetEdgePosInSquareArea_Func(Vector2 _startPos, Vector2 _targetPos, Vector2 _areaPosMin, Vector2 _areaPosMax, Vector2 _areaPosCenter)
        {
            _areaPosMin += _areaPosCenter;
            _areaPosMax += _areaPosCenter;

            Vector2 _targetDir = (_targetPos - _startPos).normalized;
            ReachFieldEdgeDir _reachFieldEdgeDir = 0f < _targetDir.x
                ? 0f < _targetDir.y
                    ? ReachFieldEdgeDir.Right_Up
                    : ReachFieldEdgeDir.Right_Down
                : 0f < _targetDir.y
                    ? ReachFieldEdgeDir.Left_Up
                    : ReachFieldEdgeDir.Left_Down;

            float _remainDistanceX = 0f;
            float _remainDistanceY = 0f;
            float _distanceX = 0f;
            float _distanceY = 0f;

            switch (_reachFieldEdgeDir)
            {
                case ReachFieldEdgeDir.Right_Up:
                    _remainDistanceX = _areaPosMax.x - _targetPos.x;
                    _remainDistanceY = _areaPosMax.y - _targetPos.y;
                    break;

                case ReachFieldEdgeDir.Right_Down:
                    _remainDistanceX = _areaPosMax.x - _targetPos.x;
                    _remainDistanceY = _targetPos.y - _areaPosMin.y;
                    break;

                case ReachFieldEdgeDir.Left_Up:
                    _remainDistanceX = _targetPos.x - _areaPosMin.x;
                    _remainDistanceY = _areaPosMax.y - _targetPos.y;
                    break;

                case ReachFieldEdgeDir.Left_Down:
                    _remainDistanceX = _targetPos.x - _areaPosMin.x;
                    _remainDistanceY = _targetPos.y - _areaPosMin.y;
                    break;

                default:
                    Debug_C.Error_Func("_reachFieldEdgeDir : " + _reachFieldEdgeDir);
                    break;
            }

            _remainDistanceX = Mathf.Abs(_remainDistanceX);
            _remainDistanceY = Mathf.Abs(_remainDistanceY);

            _distanceX = _remainDistanceX / _targetDir.x;
            _distanceY = _remainDistanceY / _targetDir.y;

            _distanceX = Mathf.Abs(_distanceX);
            _distanceY = Mathf.Abs(_distanceY);

            bool _isClosePosX = _distanceX < _distanceY;

            switch (_reachFieldEdgeDir)
            {
                case ReachFieldEdgeDir.Right_Up:
                    _targetPos = _isClosePosX == true
                        ? _targetPos = new Vector2(_targetPos.x + _remainDistanceX, _targetPos.y + (_targetDir.y * _distanceX))
                        : _targetPos = new Vector2(_targetPos.x + (_targetDir.x * _distanceY), _targetPos.y + _remainDistanceY);
                    break;

                case ReachFieldEdgeDir.Right_Down:
                    _targetPos = _isClosePosX == true
                        ? _targetPos = new Vector2(_targetPos.x + _remainDistanceX, _targetPos.y + (_targetDir.y * _distanceX))
                        : _targetPos = new Vector2(_targetPos.x + (_targetDir.x * _distanceY), _targetPos.y - _remainDistanceY);
                    break;

                case ReachFieldEdgeDir.Left_Up:
                    _targetPos = _isClosePosX == true
                        ? _targetPos = new Vector2(_targetPos.x - _remainDistanceX, _targetPos.y + (_targetDir.y * _distanceX))
                        : _targetPos = new Vector2(_targetPos.x + (_targetDir.x * _distanceY), _targetPos.y + _remainDistanceY);
                    break;

                case ReachFieldEdgeDir.Left_Down:
                    _targetPos = _isClosePosX == true
                        ? _targetPos = new Vector2(_targetPos.x - _remainDistanceX, _targetPos.y + (_targetDir.y * _distanceX))
                        : _targetPos = new Vector2(_targetPos.x + (_targetDir.x * _distanceY), _targetPos.y - _remainDistanceY);
                    break;

                default:
                    Debug_C.Error_Func("_reachFieldEdgeDir : " + _reachFieldEdgeDir);
                    break;
            }

            return _targetPos;
        }
        private enum ReachFieldEdgeDir
        {
            None = 0,

            Right_Up,
            Right_Down,
            Left_Up,
            Left_Down,
        }

        public static bool CheckDistance_Func(ref Vector2 _leftPos, ref Vector2 _rightPos, ref float _innerDist)
        {
            float xDiff = _leftPos.x - _rightPos.x;
            float yDiff = _leftPos.y - _rightPos.y;

            return (xDiff * xDiff + yDiff * yDiff) <= (_innerDist * _innerDist);
        }
        public static bool CheckDistance_Func(ref Vector3 _leftPos, ref Vector3 _rightPos, ref float _innerDist)
        {
            float xDiff = _leftPos.x - _rightPos.x;
            float yDiff = _leftPos.y - _rightPos.y;
            float zDiff = _leftPos.z - _rightPos.z;

            return (xDiff * xDiff + yDiff * yDiff + zDiff * zDiff) <= (_innerDist * _innerDist);
        }
    }
    #endregion
    #region RichText_C
    public static class RichText_C
    {
        private const string BoldStart = "<b>";
        private const string BoldEnd = "</b>";
        private const string SizeStart = "<size=";
        private const string SizeEnd = "</size>";
        private const string ColorStart = "<color=#";
        private const string ColorEnd = "</color>";

        private const string StartCut = ">";

        public static string SetBold_Func(string _str)
        {
            return StringBuilder_C.Append_Func(BoldStart, _str, BoldEnd);
        }
        public static string SetSize_Func(string _str, int _size)
        {
            string _sizeStr = _size.ToString();
            return StringBuilder_C.Append_Func(SizeStart, _sizeStr, StartCut, _str, SizeEnd);
        }
        public static string SetColor_Func(string _str, Color _color)
        {
            string _colorStr = ColorUtility.ToHtmlStringRGB(_color);
            return StringBuilder_C.Append_Func(ColorStart, _colorStr, StartCut, _str, ColorEnd);
        }
    }
    #endregion
    #region Enum_C
    public static class Enum_C
    {
        public static T[] GetEnumItemAll_Func<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>().ToArray();
        }
        public static T[] GetEnumItemAll_Func<T>(params T[] _exceptArr) where T : System.Enum
        {
            int[] _exceptIdArr = new int[_exceptArr.Length];

            for (int i = 0; i < _exceptArr.Length; i++)
            {
                _exceptIdArr[i] = _exceptArr[i].ToInt();
            }

            return GetEnumItemAll_Func<T>(_exceptIdArr);
        }
        public static T[] GetEnumItemAll_Func<T>(params int[] _exceptIdArr) where T : System.Enum
        {
            List<T> _list = new List<T>();

            T[] _arr = Enum_C.GetEnumItemAll_Func<T>();
            foreach (T _item in _arr)
            {
                int _itemID = _item.ToInt();
                bool _isExcept = false;

                foreach (var _exceptId in _exceptIdArr)
                {
                    if (_itemID == _exceptId)
                    {
                        _isExcept = true;
                        break;
                    }
                }

                if (_isExcept == true)
                    continue;

                _list.Add(_item);
            }

            return _list.ToArray();
        }
    }
    #endregion
    #region Debug_C
    public static partial class Debug_C
    {
        private const string Colon = " : ";

        private static IDebug_C iDebugC;

        public static void Init_Func(IDebug_C _iDebugC) => Debug_C.iDebugC = _iDebugC;
        public static bool IsLogType_Func(PrintLogType _logType) => Debug_C.iDebugC == null ? false : Debug_C.iDebugC.IsLogType_Func(_logType);

        [System.Diagnostics.Conditional("Test_Cargold")]
        public static void Log_Func(string _str, PrintLogType _logType = PrintLogType.Common)
        {
#if UNITY_EDITOR
            if (IsLogType_Func(_logType) == true)
                Log();
#else
            Log();
#endif

            void Log()
            {
                _str = StringBuilder_C.Append_Func(_logType.ToString(), Colon, _str);
                Debug.Log(_str);
            }
        }

        [System.Diagnostics.Conditional("Test_Cargold")]
        public static void Warning_Func(string _str, PrintLogType _logType = PrintLogType.Common)
        {
#if UNITY_EDITOR
            if (IsLogType_Func(_logType) == true)
                Log();
#else
            Log();
#endif

            void Log()
            {
                _str = StringBuilder_C.Append_Func(_logType.ToString(), Colon, _str);
                Debug.LogWarning(_str);
            }
        }

        [System.Diagnostics.Conditional("Test_Cargold")]
        public static void Error_Func(string _str, PrintLogType _logType = PrintLogType.Common)
        {
#if UNITY_EDITOR
            if (IsLogType_Func(_logType) == true)
                Log();
#else
            Log();
#endif

            void Log()
            {
                _str = StringBuilder_C.Append_Func(_logType.ToString(), Colon, _str);
                Debug.LogError(_str);
            }
        }

        public interface IDebug_C
        {
            bool IsLogType_Func(PrintLogType _logType = PrintLogType.Common);
        }
    }
    #endregion
    #region Spine_C
#if Spine_C
namespace Spine_C
{
    using Spine;
    using Spine.Unity;

    public static class Spine_C
    {
        public static void SetSkeletonSlotColor_Func(this Skeleton _skeleton, string _slotName, Color _color)
        {
#if UNITY_EDITOR
            try
            {
                Set_Func();
            }
            catch (Exception _e)
            {

                Debug_C.Error_Func("Msg : " + _e.Message + " / _skeleton : " + _skeleton + " / _slotName : " + _slotName);
            }
#else
            Set_Func();
#endif

            void Set_Func()
            {
                Spine.Slot _slot = _skeleton.FindSlot(_slotName);
                Spine.Unity.SkeletonExtensions.SetColor(_slot, _color);
            }
        }

        public static void SetSkeletonSlotAttach_Func(this Skeleton _skeleton, string _slotName, string _attachmentName)
        {
            _skeleton.SetAttachment(_slotName, _attachmentName);
        }

        public static void SetAni_Func(this SkeletonGraphic _sg, Animation _playAni, Animation _nextAni)
        {
            AnimationState _aniState = _sg.AnimationState;
            _aniState.SetAnimation(0, _playAni, false);
            _aniState.AddAnimation(0, _nextAni, true, 0f);
        }

        public enum BonePosType
        {
            World,
            Local,
            Skeleton
        }
    }

    [System.Serializable]
    public abstract class SpineController
    {
        private Dictionary<string, Bone> strByBoneDic;

        public abstract Skeleton GetSkeleton { get; }

        public virtual void Init_Func()
        {
            this.strByBoneDic = new Dictionary<string, Bone>();
            foreach (var item in strByBoneDic)
            {
                Debug_C.Log_Func(item.Value.Data.Name);
            }

            Skeleton _skeleton = this.GetSkeleton;
            if(_skeleton != null)
            {
                ExposedList<Bone> _boneList = _skeleton.Bones;
                foreach (Bone _bone in _boneList)
                    this.strByBoneDic.Add_Func(_bone.Data.Name, _bone);
            }
        }

        public abstract void PlayAni_Func(string _aniName, bool _isLoop = false, float _aniSpeed = 1f);

        public abstract Spine.Animation GetSpineAni_Func(string _aniName);
        public abstract void SetAniSpeed_Func(float _aniSpeed);
        public bool TryGetBone_Func(string _boneName, out Bone _bone)
        {
            return this.strByBoneDic.TryGetValue(_boneName, out _bone);
        }

        public abstract void Reload_Func(SkeletonDataAsset _skeletonDataAsset);

        public abstract void Subscribe_AniComplete_Func(Action<Spine.TrackEntry> _del);
        public abstract void Unsubscribe_AniComplete_Func(Action<Spine.TrackEntry> _del);
        public abstract void UnsubscribeAll_Func();
    }

    [System.Serializable]
    public class SpineController<T> : SpineController where T : MonoBehaviour, ISkeletonComponent, IHasSkeletonDataAsset, IAnimationStateComponent
    {
        [SerializeField] private T spineClass;
#if ODIN_INSPECTOR
        [Sirenix.OdinInspector.ReadOnly, Sirenix.OdinInspector.ShowInInspector]
#endif
        public Spine.Skeleton skeleton = null;
#if ODIN_INSPECTOR
        [Sirenix.OdinInspector.ReadOnly, Sirenix.OdinInspector.ShowInInspector]
#endif
        private Spine.AnimationState spineAnimationState;
        private Observer.Observer_Action<Spine.TrackEntry> aniCompleteObs;

        public override Skeleton GetSkeleton => this.skeleton;

        public void Init_Func(T _spineClass, bool _isInit = true)
        {
            this.spineClass = _spineClass;

            if (_isInit == true)
                this.Init_Func();
        }
        public override void Init_Func()
        {
            base.Init_Func();

            if (this.spineClass is SkeletonAnimation == true)
            {
                SkeletonAnimation _skeletonAnimation = this.spineClass as SkeletonAnimation;
                _skeletonAnimation.Initialize(true);
            }
            else if (this.spineClass is SkeletonGraphic == true)
            {
                SkeletonGraphic _skeletonGraphic = this.spineClass as SkeletonGraphic;
                _skeletonGraphic.Initialize(true);
            }
            else
            {
                Debug_C.Error_Func("?");
            }

            Skeleton _skeleton = this.spineClass.Skeleton;
            this.skeleton = _skeleton;
            this.spineAnimationState = this.spineClass.AnimationState;

            this.spineAnimationState.Complete += CallDel_Notify_AniComplete_Func;
            this.aniCompleteObs = new Cargold.Observer.Observer_Action<Spine.TrackEntry>();
        }

        public override void PlayAni_Func(string _aniName, bool _isLoop = false, float _aniSpeed = 1f)
        {
            this.spineAnimationState.SetAnimation(0, _aniName, _isLoop);

            this.SetAniSpeed_Func(_aniSpeed);
        }

        public override Spine.Animation GetSpineAni_Func(string _aniName)
        {
            Spine.Animation _ani = this.skeleton.Data.FindAnimation(_aniName);
            return _ani;
        }
        public Transform GetSpineTrf_Func()
        {
            return this.spineClass.transform;
        }
        public Vector2 GetBonePos_Func(string _boneName, Spine_C.BonePosType _bonePosType)
        {
            if(base.TryGetBone_Func(_boneName, out Bone _bone) == true)
            {
                switch (_bonePosType)
                {
                    case Spine_C.BonePosType.World:     return _bone.GetWorldPosition(this.spineClass.transform);
                    case Spine_C.BonePosType.Local:     return _bone.GetLocalPosition();
                    case Spine_C.BonePosType.Skeleton:  return _bone.GetSkeletonSpacePosition();
                }
            }

            return default;
        }
        public override void SetAniSpeed_Func(float _aniSpeed)
        {
            this.spineAnimationState.TimeScale = _aniSpeed;
        }

        public override void Reload_Func(SkeletonDataAsset _skeletonDataAsset)
        {
            if (this.spineClass is SkeletonAnimation == true)
            {
                SkeletonRenderer _skeletonRenderer = this.spineClass as SkeletonRenderer;
                _skeletonRenderer.skeletonDataAsset = _skeletonDataAsset;
                _skeletonRenderer.Initialize(true);
            }
            else if (this.spineClass is SkeletonGraphic == true)
            {
                SkeletonGraphic _skeletonGraphic = this.spineClass as SkeletonGraphic;
                _skeletonGraphic.skeletonDataAsset = _skeletonDataAsset;
                _skeletonGraphic.Initialize(true);
            }
            else
            {
                Debug_C.Error_Func("?");
            }

            //Spine_C.Custom.EditorForceReloadSkeletonDataAssetAndComponent(_skeletonRenderer);
        }

        public override void Subscribe_AniComplete_Func(Action<Spine.TrackEntry> _del)
        {
            this.aniCompleteObs.Subscribe_Func(_del);
        }
        public override void Unsubscribe_AniComplete_Func(Action<Spine.TrackEntry> _del)
        {
            this.aniCompleteObs.Unsubscribe_Func(_del);
        }
        public override void UnsubscribeAll_Func()
        {
            this.aniCompleteObs.UnsubscribeAll_Func();
        }
        private void CallDel_Notify_AniComplete_Func(Spine.TrackEntry _trackEntry)
        {
            this.aniCompleteObs.Notify_Func(_trackEntry);
        }

        private enum SpineClassType
        {
            None,
            SkeletonAnimation,
            SkeletonGraphic,
        }
    }

    [System.Serializable]
    public class SpineControler_Ingame : SpineController<SkeletonAnimation>
    {
        public Vector2 GetBonePos_Func(string _boneName)
        {
            return base.GetBonePos_Func(_boneName, Spine_C.BonePosType.World);
        }
    }

    [System.Serializable]
    public class SpineControler_UI : SpineController<SkeletonGraphic>
    {
        public Vector2 GetBonePos_Func(string _boneName)
        {
            // Loc Pos에 넣는게 좋음

            return base.GetBonePos_Func(_boneName, Spine_C.BonePosType.Local);
        }
    }
}
#endif
    #endregion
    #region Asset_C
#if UNITY_EDITOR
    public static class Asset_C
    {
        public static T GetLoadAssetAtPath_Func<T>(string _path) where T : UnityEngine.Object
        {
            T _asset = UnityEditor.AssetDatabase.LoadAssetAtPath<T>(_path);
            if (_asset == null)
                Debug.LogError(_path + "경로의 " + typeof(T) + "을 못 불러옴 ~");

            return _asset;
        }
    }
#endif
    #endregion
    #region DataStructure_C
    public class DataStructure_C
    {
        public static Dictionary<K, List<V>> Add_Func<K, V>(Dictionary<K, List<V>> _dic, K _key, V _value) where K : struct, IConvertible
        {
            if (_dic == null)
                _dic = new Dictionary<K, List<V>>(EnumCompare.EnumCompare<K>.Instance);

            if (_dic.TryGetValue(_key, out List<V> _list) == false)
            {
                _list = new List<V>();
                _dic.Add(_key, _list);
            }

            _list.AddNewItem_Func(_value);

            return _dic;
        }

        public static Dictionary<K, V[]> GetListToArr_Func<K, V>(Dictionary<K, List<V>> _dic, Dictionary<K, V[]> _resultDic = null) where K : struct, IConvertible
        {
            if (_resultDic == null)
                _resultDic = new Dictionary<K, V[]>(EnumCompare.EnumCompare<K>.Instance);

            foreach (var item in _dic)
            {
                K _key = item.Key;
                List<V> _list = item.Value;
                _resultDic.Add(_key, _list.ToArray());
            }

            return _resultDic;
        }
    }
    #endregion
    #region GameDataEditor
#if GameDataEditor
namespace GameDataEditor
{
    public partial class GDEDataManager
    {
        public static GDEDataManager Instance
        {
            get
            {
                if (_instance == null || dataDic == null)
                    Init_Func();

                return _instance;
            }
        }
        private static GDEDataManager _instance;

        private static Dictionary<string, IGDEData> dataDic;

        private DateTime editorSyncTime;

        public static void Init_Func(string _gdePath = null)
        {
            _instance = new GDEDataManager();

            if (_gdePath.IsNullOrWhiteSpace_Func() == true)
                _gdePath = "gde_data";

            if (dataDic == null)
                dataDic = new Dictionary<string, IGDEData>();

            GDEDataManager.Init(_gdePath);
        }

        public List<T> GetData_Func<T>() where T : IGDEData
        {
            if (Instance == null)
                Init_Func();

            List<T> _gdeDataList = GDEDataManager.GetAllItems<T>();
            return _gdeDataList;
        }

        public T GetData_Func<T>(string _key) where T : IGDEData
        {
            T _resultData = null;

            if(this.editorSyncTime < DateTime.Now && _key.IsNullOrWhiteSpace_Func() == false)
            {
                this.editorSyncTime = this.editorSyncTime.AddSeconds(0.5d);

                if (dataDic.ContainsKey(_key) == false)
                {
                    List<T> _gdeDataList = GDEDataManager.GetAllItems<T>();

                    foreach (T _gdeData in _gdeDataList)
                    {
                        if (dataDic.ContainsKey(_gdeData.Key) == false)
                            dataDic.Add(_gdeData.Key, _gdeData);
                    }
                }

                if(dataDic.ContainsKey(_key) == true)
                    _resultData = dataDic.GetValue_Func(_key) as T;
            }

            return _resultData;
        }
        public bool TryGetData_Func<T>(string _key, out T _gdeData) where T : IGDEData
        {
            _gdeData = this.GetData_Func<T>(_key);
            return _gdeData != null;
        }
    }
}
#endif
    #endregion

    // Developing System
    #region TextPrint_Manager
    namespace TextPrint
    {
        //public class TextPrint_Manager : MonoBehaviour
        //{
        //    public static TextPrint_Manager Instance;

        //    [SerializeField] private Color printColor;
        //    [SerializeField] private float punchTime;
        //    [SerializeField] private float printSize;
        //    [SerializeField] private float printTime;
        //    [SerializeField] private float clearTime;
        //    public Color PrintColor { get { return printColor; } }
        //    public float PunchTime { get { return punchTime; } }
        //    public float PrintSize { get { return printSize; } }
        //    public float PrintTime { get { return printTime; } }
        //    public float ClearTime { get { return clearTime; } }
        //    public static Color _a;

        //    [SerializeField] private Transform dpGroupTrf;
        //    private List<TextPrint_Script> dpList;
        //    [SerializeField] private GameObject dpObj;

        //    public void Init_Func()
        //    {
        //        Instance = this;

        //        dpList = new List<TextPrint_Script>();
        //        for (int i = 0; i < 10; i++)
        //        {
        //            GenerateDP_Func();
        //        }
        //    }
        //    private TextPrint_Script GenerateDP_Func()
        //    {
        //        GameObject _dpObj = Instantiate(dpObj);
        //        _dpObj.transform.SetParent(dpGroupTrf);

        //        TextPrint_Script _dpClass = _dpObj.GetComponent<TextPrint_Script>();
        //        _dpClass.Init_Func();
        //        dpList.Add(_dpClass);
        //        _dpObj.SetActive(false);

        //        return _dpClass;
        //    }

        //    public void Print_Func(Vector2 _pos, string _value, Sprite _sprite = null)
        //    {
        //        Print_Func(_pos, _value, PrintColor, _sprite);
        //    }
        //    public void Print_Func(Vector2 _pos, float _value)
        //    {
        //        Print_Func(_pos, _value, PrintColor);
        //    }
        //    public void Print_Func(Vector2 _pos, float _value, Color _color)
        //    {
        //        Print_Func(_pos, ((int)_value).ToString(), _color);
        //    }
        //    public void Print_Func(Vector2 _pos, string _value, Color _color, Sprite _sprite = null, params float[] _varArr)
        //    {
        //        TextPrint_Script _textPrintClass = null;
        //        if (0 < dpList.Count)
        //        {
        //            _textPrintClass = this.dpList[0];
        //            this.dpList.RemoveAt(0);
        //        }
        //        else
        //        {
        //            _textPrintClass = GenerateDP_Func();
        //        }

        //        _textPrintClass.Print_Func(_pos, _value, _color, null, _varArr);
        //    }

        //    public void PrintOver_Func(TextPrint_Script _textPrintClass)
        //    {
        //        this.dpList.Add(_textPrintClass);
        //    }
        //}
        //public class TextPrint_Script : MonoBehaviour
        //{
        //    public Text damageText;
        //    private float punchTime;
        //    private float printSize;
        //    private float printTime;
        //    private float clearTime;
        //    [SerializeField]
        //    private Image printImage;

        //    public void Init_Func()
        //    {
        //        this.gameObject.SetActive(false);
        //    }
        //    public void Print_Func(Vector2 _pos, string _value, Color _color, Sprite _sprite = null, params float[] _varArr)
        //    {
        //        if (_sprite != null)
        //        {
        //            printImage.SetFade_Func(1f);
        //            printImage.SetNativeSize_Func(_sprite);
        //        }

        //        if (_varArr.Length != 4)
        //        {
        //            punchTime = TextPrint_Manager.Instance.PunchTime;
        //            printSize = TextPrint_Manager.Instance.PrintSize;
        //            printTime = TextPrint_Manager.Instance.PrintTime;
        //            clearTime = TextPrint_Manager.Instance.ClearTime;
        //        }
        //        else
        //        {
        //            punchTime = 0f < _varArr[0] ? _varArr[0] : TextPrint_Manager.Instance.PunchTime;
        //            printSize = 0f < _varArr[1] ? _varArr[1] : TextPrint_Manager.Instance.PrintSize;
        //            printTime = 0f < _varArr[2] ? _varArr[2] : TextPrint_Manager.Instance.PrintTime;
        //            clearTime = 0f < _varArr[3] ? _varArr[3] : TextPrint_Manager.Instance.ClearTime;
        //        }

        //        this.gameObject.SetActive(true);

        //        damageText.text = _value;
        //        damageText.color = _color;

        //        this.transform.position = _pos;
        //        this.transform.localScale = Vector3.zero;
        //        this.transform.DOScale(Vector3.one * printSize, punchTime);

        //        damageText.DOColor(_color, printTime).OnComplete(DoClear_Func);
        //    }

        //    public void DoClear_Func()
        //    {
        //        damageText.DOColor(Color.clear, clearTime);

        //        this.transform.DOScale(Vector3.zero, clearTime).OnComplete(PrintOver_Func);
        //    }

        //    public void PrintOver_Func()
        //    {
        //        if (printImage.sprite != null)
        //        {
        //            printImage.sprite = null;
        //            printImage.SetFade_Func(0f);
        //        }

        //        this.gameObject.SetActive(false);

        //        TextPrint_Manager.Instance.PrintOver_Func(this);
        //    }
        //}
    }
    #endregion
    #region Abstract Data
    // 용도 : 부모 인터페이스에서 어느 타입인지 확인하고서 적합한 타입으로 다운 캐스팅한 후 데이터 Get하기
    // 개선 : 밸류들을 데이터용 클래스에 기록한 뒤 인자로 주고 받으면 어떨까? 매니저가 데이터 클래스를 풀링한 뒤 관리
    namespace AbstractData
    {
        public interface IAbstractData
        {
            AbstractDataType GetAbstractDataType_Func();
        }

        public interface IAD_Int : IAbstractData
        {
            int GetAD_Int_Func();
        }

        public interface IAD_Int_2 : IAbstractData
        {
            AD_Int_2 GetAD_Int_2_Func();
        }

        public interface IAD_Float_2 : IAbstractData
        {
            AD_Float_2 GetAD_Float_2_Func();
        }

        public interface IAD_Int_Float : IAbstractData
        {
            AD_Int_Float GetAD_Int_Float_Func();
        }

        public struct AD_Int_2 { public int value1, value2; }
        public struct AD_Int_Float { public int intValue; public float floatValue; }
        public struct AD_Float_2 { public float value1, value2; }

        public enum AbstractDataType
        {
            None = 0,

            Int,
            Int_2,

            Int_Float,

            Float2,

            Vector2,

            Vector3,
        }
    }
    #endregion

    // Coming Soon...
    #region Dragger
    // Potion 게임에서 쓰던 SelectMatter를 범용적으로 모듈화하여 WhichOne처럼 쓸모있게 만들자
    // 1. 끌고 다니는게, 선택한 객체 그 자체일 수도 있고, 새로운 드래깅 객체일 수도 있고 ㅇㅇ
    // 2. 드래그의 동기화 속도를 조절 가능하게끔
    #endregion
    #region Sound System
    #endregion
    #region Trash Group
    namespace Trash
    {
        public class RaritySort
        {
            // 임의로 명명한 등급 순으로 영웅을 정렬하고 싶을 때 어떻게 하는가?

            public string[] fixRarityArr = { "SSS", "SS", "S", "AAA", "A", "B", "C", "D" };
            public List<hero_dic_info> hero_Dic_Info_Item;

            public class hero_dic_info
            {
                public string rarity;

                public hero_dic_info(string _rarity)
                {
                    this.rarity = _rarity;
                }
            }

            private void Start()
            {
                hero_Dic_Info_Item = new List<hero_dic_info>();

                this.ReadCsv_Func(this.hero_Dic_Info_Item);

                this.Sort_Func(this.hero_Dic_Info_Item);

                this.PrintDesc_Func(this.hero_Dic_Info_Item);
            }

            private void ReadCsv_Func(List<hero_dic_info> _setList)
            {
                for (int i = 0; i < 10; i++)
                {
                    int _randRarityID = UnityEngine.Random.Range(0, fixRarityArr.Length);
                    string _randRarity = fixRarityArr[_randRarityID];
                    hero_dic_info _info = new hero_dic_info(_randRarity);
                    _setList.Add(_info);
                }
            }

            private void Sort_Func(List<hero_dic_info> _sortList)
            {
                for (int x = 0; x < _sortList.Count - 1; x++)
                {
                    for (int y = x + 1; y < _sortList.Count; y++)
                    {
                        hero_dic_info _x = _sortList[x];
                        hero_dic_info _y = _sortList[y];

                        int _xRarityID = this.GetRarityID_Func(_x);
                        int _yRarityID = this.GetRarityID_Func(_y);

                        if (_yRarityID < _xRarityID)
                        {
                            this.SwapHero_Func(ref _x, ref _y);

                            _sortList[x] = _x;
                            _sortList[y] = _y;

                            continue;
                        }
                        else
                        {

                        }
                    }
                }
            }

            private int GetRarityID_Func(hero_dic_info _heroInfoClass)
            {
                for (int _rarity = 0; _rarity < this.fixRarityArr.Length; _rarity++)
                {
                    if (_heroInfoClass.rarity != this.fixRarityArr[_rarity])
                    {

                    }
                    else
                    {
                        return _rarity;
                    }
                }

                Debug_C.Error_Func("해당 등급이 없다능");

                return -1;
            }

            private void SwapHero_Func(ref hero_dic_info _x, ref hero_dic_info _y)
            {
                hero_dic_info _temp = _x;

                _x = _y;

                _y = _temp;
            }

            private void PrintDesc_Func(List<hero_dic_info> _printList)
            {
                for (int i = 0; i < _printList.Count; i++)
                {
                    Debug.Log(i + " / " + _printList[i].rarity);
                }
            }
        }
    }
    #endregion
    #region Wrapping
    namespace Wrapping
    {
        // GC 없이 Enum을 Int로 캐스팅하는 내용인데, 다른 용도로도 쓸 수 있을 듯?

        class WrapperObject<TEnum, TValue>
        {
            TValue[] data;

            static Dictionary<TEnum, int> _enumKey = new Dictionary<TEnum, int>();

            static WrapperObject()
            {
                int[] intValues = Enum.GetValues(typeof(TEnum)) as int[];
                TEnum[] enumValues = Enum.GetValues(typeof(TEnum)) as TEnum[];

                for (int i = 0; i < intValues.Length; i++)
                {
                    _enumKey.Add(enumValues[i], intValues[i]);
                }
            }

            public WrapperObject(int count)
            {
                data = new TValue[count];
            }

            public TValue this[TEnum key]
            {
                get { return data[_enumKey[key]]; }
                set { data[_enumKey[key]] = value; }
            }
        }
    }
    #endregion
    #region Sort
    namespace Sort
    {
        // 정렬 알고리즘 ㄱㄱ
    }
    #endregion 
}