﻿namespace Cargold
{
    public static partial class Debug_C
    {
        [System.Flags]
        public enum PrintLogType
        {
            None = 0,

            Jump = 1 << 1,
            Falling = 1 << 2,
            Collide = 1 << 3,
            View = 1 << 4,
            Attack = 1 << 5,
            Move = 1 << 6,
            Camera = 1 << 7,

            #region FrameWork
            Common = 1 << 1000,
            Save = 1 << 1001,
            #endregion

            All = ~None,
        }
    } 
}