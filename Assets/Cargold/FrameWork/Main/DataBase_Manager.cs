﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cargold;
using static Cargold.Debug_C;

namespace Cargold.FrameWork
{
    public abstract class DataBase_Manager : MonoBehaviour, GameSystem_Manager.IInitialize, IDebug_C
    {
        public const string FrameWork = "프레임워크";
        public const string Mandatory = "필수";
        public const string Optional = "선택";
        public const string Fomula = "수식";

        public static DataBase_Manager Instance;

        //public abstract Define GetDefine { get; }
        //public abstract Localize GetLocalize { get; }
        //public abstract UI GetUi { get; }
        //public abstract Sound GetSound { get; }
        //public abstract Log GetLog { get; }
        protected abstract PrintLogType GetPrintLogType { get; }

        public virtual void Init_Func(int _layer)
        {
            if (_layer == 0)
            {
                Debug_C.Init_Func(this);

                Instance = this;

                //this.GetDefine?.Init_Func();
                //this.GetLocalize?.Init_Func();
                //this.GetUi?.Init_Func();
            }
            else if (_layer == 1)
            {

            }
        }
        public bool IsLogType_Func(PrintLogType _logType = PrintLogType.Common)
        {
            return GetPrintLogType.HasFlag(_logType);
        }

        /*
        #region Template
        [System.Serializable]
        public abstract class Template : MonoBehaviour
        {
            public virtual void Init_Func()
            {

            }
        }
        #endregion
        #region Define
        [System.Serializable]
        public class Define : Template
        {
            public const string DefineStr = "디파인";

            [FoldoutGroup(FrameWork), LabelText("세이브 기능 활성화")] public bool isSaveActivate;
            [FoldoutGroup(FrameWork), HorizontalGroup(FrameWork + "/1"), BoxGroup(FrameWork + "/1/일"), HideLabel] public int testAddDay;
            [FoldoutGroup(FrameWork), HorizontalGroup(FrameWork + "/1"), BoxGroup(FrameWork + "/1/시"), HideLabel] public int testAddHour;
            [FoldoutGroup(FrameWork), HorizontalGroup(FrameWork + "/1"), BoxGroup(FrameWork + "/1/분"), HideLabel] public int testAddMin;
            [FoldoutGroup(FrameWork), HorizontalGroup(FrameWork + "/1"), BoxGroup(FrameWork + "/1/초"), HideLabel] public int testAddSec;
        }
        #endregion
        #region Localize
        [System.Serializable]
        public abstract class Localize : Template
        {
            public const string LczStr = "로컬라이즈";

            public abstract string GetLcz_Func(string _lczID, SystemLanguage _languageType);
        }
        #endregion
        #region UI
        [System.Serializable]
        public abstract class UI : Template
        {
            public const string UIStr = "유아이";

            [FoldoutGroup(FrameWork), BoxGroup("버튼"), LabelText("In Twn 시간")] public float btnScaleDownTime = .1f;
            [FoldoutGroup(FrameWork), BoxGroup("버튼"), LabelText("In Twn 크기 감소율")] public float btnScaleDown = .1f;
            [FoldoutGroup(FrameWork), BoxGroup("버튼"), LabelText("Out Twn 시간")] public float btnPunchTime = .4f;

            [FoldoutGroup(FrameWork), BoxGroup("트윈"), LabelText("세기")] public float twn_Power = 1f;
            [FoldoutGroup(FrameWork), BoxGroup("트윈"), LabelText("지속시간")] public float twn_Duraion = .25f;

            [FoldoutGroup(FrameWork), BoxGroup("지속 터치"), LabelText("시작 지연")] public float continuousBtn_BeginDelay = .25f;
            [FoldoutGroup(FrameWork), BoxGroup("지속 터치"), LabelText("지연 감속 간격")] public float continuousBtn_DecreaseInterval = .02f;
            [FoldoutGroup(FrameWork), BoxGroup("지속 터치"), LabelText("최소 지연 간격")] public float continuousBtn_MaxInterval = .02f;
            [FoldoutGroup(FrameWork), BoxGroup("지속 터치/알림"), LabelText("발동 횟수 누적 조건")] public int continuousBtn_StackCount = 4;
            [FoldoutGroup(FrameWork), BoxGroup("지속 터치/알림"), LabelText("발동 횟수 차감 지연 시간")] public float continuousBtn_StackDelay = .5f;
        }
        #endregion
        #region Sound
        [System.Serializable]
        public abstract class Sound : Template
        {
            [TableList(AlwaysExpanded = true, DrawScrollView = false, ShowPaging = false, ShowIndexLabels = true), FoldoutGroup("Bgm 테이블"), HideLabel]
            public BgmData[] bgmDataArr;
            [TableList(AlwaysExpanded = true, DrawScrollView = false, ShowPaging = false, ShowIndexLabels = true), FoldoutGroup("Sfx 테이블"), HideLabel]
            public SfxData[] sfxDataArr;

            [LabelText("배경음 페이드 속도")] public float bgmFadeSpeed = 1f;

            [System.Serializable]
            public class Record
            {
                [VerticalGroup("종류"), HideLabel, PropertyOrder(-1)]
                [InfoBox("Key 지정하셈!", "IsKeyNone", InfoMessageType = InfoMessageType.Error)]
                public string sourceKey = string.Empty;
                private bool IsKeyNone => this.sourceKey.IsNullOrWhiteSpace_Func();

                [VerticalGroup("클립"), HideLabel]
                [InfoBox("클립 추가하셈!", "IsClipNull", InfoMessageType = InfoMessageType.Error)]
                public AudioClip clip;
                private bool IsClipNull { get { return this.clip == null; } }

                [VerticalGroup("볼륨"), HideLabel]
                [ProgressBar(0f, 1f)]
                public float volume = .5f;
            }

            [System.Serializable]
            public class BgmData : Record
            {

            }

            [System.Serializable]
            public class SfxData : Record
            {
                [VerticalGroup("테스트 ㄱㄱ~"), Button("꾹")]
                private void CallEdit_Play_Func()
                {
                    Cargold.FrameWork.SoundSystem_Manager.PlaySfx_Func(this);
                }
            }
        }
        #endregion
        #region Log
        [System.Serializable]
        public abstract class Log : Template
        {
            [FoldoutGroup(FrameWork), BoxGroup(FrameWork + "/플레이 타임"), LabelText("코루틴 간격(초)")] public float playTimeSecInterval = 10;
            [FoldoutGroup(FrameWork), BoxGroup(FrameWork + "/플레이 타임"), LabelText("최대 시간(분)")] public int playTimeMinMax = 120;
            [FoldoutGroup(FrameWork), BoxGroup(FrameWork + "/플레이 타임"), LabelText("기록 간격(분)")] public int playTimeMinInterval = 5;
        }
        #endregion
        */

        public static void DataError_Func(string _key, System.Exception _e)
        {
            Debug_C.Error_Func("DB Load 오류) base.Key : " + _key);
            Debug_C.Warning_Func("_e Data : " + _e.Data);
            Debug_C.Warning_Func("_e Message : " + _e.Message);
            Debug_C.Warning_Func("_e Source : " + _e.Source);
            Debug_C.Warning_Func("_e String : " + _e.ToString());
        }

        public interface IData
        {
            string GetKey { get; }
        }
    }
}