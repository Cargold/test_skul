﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cargold.FrameWork
{
    public class GameSystem_Manager : MonoBehaviour
    {
        [SerializeField] protected IInitialize[] iInitArr = new IInitialize[0];
        [SerializeField] private Reporter reporterClass = null;
        [SerializeField] private bool isAutoInit = true;

        private void Awake()
        {
            if(this.isAutoInit == true)
            {
                this.Init_Func();
            }
        }

        protected virtual void Init_Func()
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;

            // 0 : 스스로를 초기화할 때
            // 1 : 외부 매니저 클래스에 접근할 때
            // 2 : 유저 데이터에 접근할 때
            for (int _layer = 0; _layer < 3; _layer++)
            {
                foreach (IInitialize _iInit in this.iInitArr)
                    _iInit.Init_Func(_layer); 
            }

#if !Test_Cargold
            GameObject.Destroy(reporterClass.gameObject);
#endif

            GameObject.DontDestroyOnLoad(this.gameObject);
        }

        public interface IInitialize
        {
            void Init_Func(int _layer);
        }
    } 
}