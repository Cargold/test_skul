﻿using UnityEngine;
using System;
using System.Collections;

public class TimeSystem_Manager : Cargold.TimeSystem.TimeSystem_Manager
{
    public TimeSystem_Manager() : base()
    {
        UnbiasedTimeManager.UnbiasedTime.Init();
        GameObject _timeObj = UnbiasedTimeManager.UnbiasedTime.Instance.gameObject;
        GameObject.DontDestroyOnLoad(_timeObj);

        Cargold.TimeSystem.TimeSystem_Manager.instance = this;
    }

    public override DateTime Now
    {
        get
        {
            if (UnbiasedTimeManager.UnbiasedTime.Instance.failed == true)
            {
                UnbiasedTimeManager.UnbiasedTime.Instance.Initialize();
            }

            DateTime _nowTime = UnbiasedTimeManager.UnbiasedTime.Instance.dateTime + DateTimeOffset.Now.Offset;
#if UNITY_EDITOR
            _nowTime = base.GetOffsetTime_Func(_nowTime);
#endif
            return _nowTime;
        }
    }
}